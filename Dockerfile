FROM buildpack-deps:bookworm AS base

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH=/app/
ENV DJANGO_SETTINGS_MODULE=django_settings

RUN useradd uwsgi

RUN mkdir -p /app/data
RUN chown uwsgi /app/data
VOLUME /app/data

RUN mkdir -p /app/media
RUN chown uwsgi /app/media
VOLUME /app/media

EXPOSE 8000/tcp

RUN apt-get update -q \
    && apt-get install -y --no-install-recommends \
    gettext \
    python3 \
    python3-pip \
    python3-wheel \
    uwsgi \
    uwsgi-plugin-python3 \
    python3-psycopg2 \
    python3-ldap \
    libldap-common \
    libsasl2-modules \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app/
COPY requirements.txt package.json uwsgi.ini LICENSE ./
COPY lagerregal/ lagerregal

RUN pip3 install --break-system-packages -r requirements.txt

RUN python3 -m django compilemessages -l de --settings=lagerregal.settings.development


FROM base AS static
RUN apt-get update -q && apt-get install -y --no-install-recommends npm
RUN npm install --omit=dev
RUN python3 -m django collectstatic --no-input --settings=lagerregal.settings.development


FROM base AS final
COPY --from=static /app/staticserve/ static
USER uwsgi
CMD ["uwsgi", "uwsgi.ini"]
