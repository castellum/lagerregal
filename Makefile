VIRTUAL_ENV ?= .venv

.PHONY: run
run: $(VIRTUAL_ENV)
	$(VIRTUAL_ENV)/bin/pip install -r requirements.txt
	$(VIRTUAL_ENV)/bin/pip install django-debug-toolbar
	yarn install || npm install
	$(VIRTUAL_ENV)/bin/python manage.py compilemessages -l de
	$(VIRTUAL_ENV)/bin/python manage.py migrate
	$(VIRTUAL_ENV)/bin/python manage.py populate
	$(VIRTUAL_ENV)/bin/python manage.py runserver

.PHONY: makemessages
makemessages: $(VIRTUAL_ENV)
	$(VIRTUAL_ENV)/bin/python manage.py makemessages -l de -d django --ignore=node_modules
	$(VIRTUAL_ENV)/bin/python manage.py makemessages -l de -d djangojs --ignore=node_modules

.PHONY: test
test: $(VIRTUAL_ENV)
	$(VIRTUAL_ENV)/bin/pip install coverage
	$(VIRTUAL_ENV)/bin/coverage run --branch --omit=.venv/* manage.py test
	$(VIRTUAL_ENV)/bin/coverage html --skip-covered

.PHONY: lint
lint: $(VIRTUAL_ENV)
	$(VIRTUAL_ENV)/bin/pip install ruff
	$(VIRTUAL_ENV)/bin/ruff check

$(VIRTUAL_ENV):
	python3 -m venv $(VIRTUAL_ENV)
	$(VIRTUAL_ENV)/bin/pip install -U pip
