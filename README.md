# Lagerregal

Lagerregal is a somewhat flexible inventory system. Mostly designed to work
best for my workplace, but it should be easily customizable to your needs.

## Screenshots

![Screenshot of Device Details](/screenshots/device_detail.png)

## Features

-   Inventory system for devices based on Django and Bootstrap
-   "Business" concepts for buildings, sections, departments, users
-   LDAP integration and synchronization scripts
-   Tracks edits of a device
-   Lend devices to users
-   Multi-Language support
-   Generates and prints Device Stickers based on PDF templates
-   Manage static IP addresses for legacy network environments
-   Semi-Automatic e-mail delivery (on lending, trashing etc)
-   Optional query and listing for puppet facts or opsi details of a device
-   Permission system for users and public device lists
-   Themable with bootswatch

## Manual Quickstart

The following command will install all dependencies, setup a demo database, and
start a testserver for development:

```
make run
```

After that you can point your browser to
[http://localhost:8000](http://localhost:8000) and login with "admin:admin".
