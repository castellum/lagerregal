from .base import *

DEBUG = True
PRODUCTION = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR.parent / 'db.sqlite3',
    }
}

# Make this unique, and don't share it with anybody.
SECRET_KEY = "CHANGE ME IN PRODUCTION AND DON'T COMMIT ME!"

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = BASE_DIR.parent / 'media'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = BASE_DIR.parent / 'staticserve'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INVENTORY_MANAGERS = ["inventorymanager@example.com"]

try:
    import debug_toolbar  # noqa
    INTERNAL_IPS = ['127.0.0.1', 'localhost']
    INSTALLED_APPS += ['debug_toolbar']
    MIDDLEWARE = [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ] + MIDDLEWARE
    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_COLLAPSED': True,
    }
except ImportError:
    pass
