import os
from pathlib import Path

from reportlab.lib.units import mm

BASE_DIR = Path(__file__).resolve().parent.parent

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Berlin'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = [
    BASE_DIR / 'static',
]

LOCALE_PATHS = [
    BASE_DIR / 'locale',
]

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'lagerregal.main.finders.NpmFinder',
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'reversion.middleware.RevisionMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'lagerregal.users.middleware.LanguageMiddleware',
]

ROOT_URLCONF = 'lagerregal.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'lagerregal.wsgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR / 'templates',
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'lagerregal.devices.context_processors.get_settings'
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
        },
    },
]

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'lagerregal.main',
    'lagerregal.devices',
    'lagerregal.devicetypes',
    'lagerregal.devicegroups',
    'lagerregal.devicetags',
    'lagerregal.locations',
    'lagerregal.users',
    'lagerregal.api',
    'lagerregal.mail',
    'lagerregal.history',
    'reversion',
    'rest_framework',
    'oauth2_provider',
    'django_bootstrap5',
]

LANGUAGES = [
    ('de', 'German'),
    ('en', 'English'),
]

AUTH_USER_MODEL = 'users.Lageruser'

SITE_NAME = "Lagerregal"

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}

AUTHENTICATION_BACKENDS = [
    # 'django_auth_ldap.backend.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
    'lagerregal.main.backends.LagerregalBackend',
]

NPM_PATH = BASE_DIR.parent / 'node_modules'
NPM_FILES = [
    Path('bootstrap', 'dist', 'css', 'bootstrap.min.css'),
    Path('bootstrap', 'dist', 'js', 'bootstrap.min.js'),
    Path('bootstrap', 'dist', 'js', 'bootstrap.min.js.map'),
    Path('font-awesome', 'css', 'font-awesome.min.css'),
    Path('font-awesome', 'fonts', 'FontAwesome.otf'),
    Path('font-awesome', 'fonts', 'fontawesome-webfont.eot'),
    Path('font-awesome', 'fonts', 'fontawesome-webfont.svg'),
    Path('font-awesome', 'fonts', 'fontawesome-webfont.ttf'),
    Path('font-awesome', 'fonts', 'fontawesome-webfont.woff'),
    Path('font-awesome', 'fonts', 'fontawesome-webfont.woff2'),
    Path('jquery', 'dist', 'jquery.min.js'),
    Path('jquery-ui-dist', 'jquery-ui.min.js'),
    Path('jquery-ui-dist', 'jquery-ui.min.css'),
    Path('jquery-ui-dist', 'images', 'ui-icons_444444_256x240.png'),
    Path('jquery-ui-dist', 'images', 'ui-icons_555555_256x240.png'),
    Path('jquery-ui-dist', 'images', 'ui-icons_777620_256x240.png'),
    Path('jquery-ui-dist', 'images', 'ui-icons_777777_256x240.png'),
    Path('jquery-ui-dist', 'images', 'ui-icons_cc0000_256x240.png'),
    Path('jquery-ui-dist', 'images', 'ui-icons_ffffff_256x240.png'),
    Path('datatables.net', 'js', 'jquery.dataTables.min.js'),
    Path('datatables.net-bs4', 'css', 'dataTables.bootstrap4.min.css'),
    Path('datatables.net-bs4', 'js', 'dataTables.bootstrap4.min.js'),
    Path('@popperjs', 'core', 'dist', 'umd', 'popper-lite.min.js'),
    Path('@popperjs', 'core', 'dist', 'umd', 'popper-lite.min.js.map'),
    Path('select2', 'dist', 'js', 'select2.min.js'),
    Path('select2', 'dist', 'css', 'select2.min.css'),
    Path('timeago', 'jquery.timeago.js'),
    Path('timeago', 'locales', 'jquery.timeago.de.js'),
]

PUBLIC_DEVICES_FILTER = {"tags__id__in": ["3", "17"]}

FAVICON_PATH = STATIC_URL + 'images/favicon.ico'

SELECT2_JS = ''
SELECT2_CSS = ''

TEST_RUNNER = 'lagerregal.utils.DetectableTestRunner'
TEST_MODE = False
PRODUCTION = False

OPERATING_SYSTEMS = [
    ("win", "Windows"),
    ("osx", "macOS"),
    ("linux", "Linux")
]

LABEL_PAGESIZE = (83 * mm, 25 * mm)
LABEL_ICON = "pdf_forms/minerva.jpg"
LABEL_TITLE = "Information Services & Technology"

HANDOVER_PROTOCOL_LOCATION = "pdf_forms/Leihschein.pdf"
HANDOVER_PROTOCOL_TEXT_LOCATIONS = {
    "user_name": [155, 632],
    "date": [350, 632],
    "devicetype": [155, 597],
    "manufacturer": [362, 597],
    "inventorynumber": [155, 563],
    "serialnumber": [372, 563],
    "name": [165, 530],
    "recipient_name": [210, 353],
}
TRASHED_PROTOCOL_LOCATION = "pdf_forms/geraeterueckgabe_lagerregal.pdf"
TRASHED_PROTOCOL_TEXT_LOCATIONS = {
    "department": [170, 668],
    "user_name": [170, 629],
    "date": [485, 668],
    "devicetype": [170, 588],
    "name": [170, 559],
    "manufacturer": [182, 433],
    "inventorynumber": [182, 373],
    "serialnumber": [182, 346],
    "room": [182, 323],
    "comment": [175, 150, 78],
    "checkmarks": [
        [537, 230]
    ]
}

EMAIL_SUBJECT_PREFIX = '[Lagerregal] '
