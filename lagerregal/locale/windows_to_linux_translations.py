import os
from pathlib import Path

pofiles = [
    Path(root, name)
    for root, dirs, files in os.walk(".")
    for name in files
    if ".po" in name
]

for pofile in pofiles:
    new_path = Path(pofile.parent / "django_new.po")

    with open(pofile) as pofile_handle:
        with open(new_path, "w") as new_pofile:
            for line in pofile_handle.readlines():
                if line.startswith("#:"):
                    line = line.replace(".\\", "")
                    line = line.replace("\\","/")

                new_pofile.write(line)

    new_path.replace(pofile)
