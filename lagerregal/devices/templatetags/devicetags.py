import os
import re
import urllib

from django.forms import CheckboxInput
from django.template import Library
from django.urls import reverse
from django.utils.html import format_html
from django.utils.html import mark_safe

from lagerregal.devices.models import Bookmark

register = Library()


@register.simple_tag
def get_verbose_name(object):
    return object._meta.verbose_name


@register.simple_tag
def history_compare(old, new):
    if not isinstance(new, str):
        try:
            new = new.name
        except Exception:
            try:
                new = new.username
            except Exception:
                pass
    if old != new:
        if old == "" and new != "":
            return format_html("+ <span class='diff add'>{0}</span>", new)
        elif old != "" and new == "":
            return format_html("- <span class='diff remove'>{0}</span>", new)
        else:
            return format_html("<span class='diff'>{0}</span>", new)
    else:
        return new


@register.filter("is_select")
def is_select(form_field_obj):
    return form_field_obj.field.widget.__class__.__name__ == "Select2Widget"


@register.filter("is_selectmultiple")
def is_selectmultiple(form_field_obj):
    return form_field_obj.field.widget.__class__.__name__ == "Select2MultipleWidget"


@register.filter(name="is_checkbox")
def is_checkbox(field):
    return field.field.widget.__class__.__name__ == CheckboxInput().__class__.__name__


class_re = re.compile(r'(?<=class=["\'])(.*)(?=["\'])')


@register.filter
def add_class(value, css_class):
    string = str(value)
    match = class_re.search(string)
    if match:
        m = re.search(rf"^{css_class}$|^{css_class}\s|\s{css_class}\s|\s{css_class}$", match.group(1))

        if m is not None:
            return mark_safe(class_re.sub(match.group(1) + " " + css_class, string))
    else:
        return mark_safe(string.replace(">", f' class="{css_class}">', 1))
    return value


@register.filter
def add_attr(value, attr):
    string = str(value)
    attr_name, attr_value = attr.split(",")
    return mark_safe(string.replace(">", f" {attr_name}='{attr_value}'>", 1))


@register.filter
def add_to_tag(value, new):
    string = str(value)
    return mark_safe(string.replace(">", f" {new}>", 1))


@register.filter
def get_range(value):
    return list(range(value))


@register.filter
def check_bookmark(device, user):
    return Bookmark.objects.filter(user=user.id, device=device.id).exists()


@register.filter
def get_attribute(object, attribute):
    try:
        return getattr(object, attribute)
    except AttributeError:
        return object.get(attribute)


@register.filter
def filename(value):
    return os.path.basename(value.file.name)


@register.simple_tag
def as_nested_list(factvalue):
    res = ""
    if isinstance(factvalue, dict):
        res += "<ul>"
        for key, value in factvalue.items():
            if isinstance(value, dict):
                res += f"<li>{key}<ul>"
                for key, value in value.items():
                    res += f"<li>{key} : {value}</li>"
                res += "</ul></li>"
            else:
                res += f"<li>{key} : {value}</li>"
        res += "</ul>"
    elif isinstance(factvalue, list):
        res += "<ul>"
        for item in factvalue:
            res += f"<li>{item}</li>"
        res += "</ul>"
    else:
        res += str(factvalue)

    return mark_safe(res)


@register.filter
def splitstr(arg1, arg2):
    return arg1.split(arg2)


@register.inclusion_tag("snippets/deletebutton.html")
def deletebutton(viewname, *args, simple=False):
    return {"url": reverse(viewname, args=args), "simple": simple}


@register.inclusion_tag("snippets/editbutton.html")
def editbutton(viewname, *args, simple=False):
    return {"url": reverse(viewname, args=args), "simple": simple}


@register.simple_tag(takes_context=True)
def current_url(context, *args, **kwargs):
    path = context["request"].get_full_path()
    if "?" in path:
        path, qs = path.split("?", 1)
        query = urllib.parse.parse_qs(qs)
    else:
        query = {}
    query.update(zip(args[::2], args[1::2]))
    query.update(kwargs)
    query = {key: value for key, value in query.items() if value}
    qs = urllib.parse.urlencode(query, doseq=True)
    return path + "?" + qs


@register.filter
def choicefield_value(boundfield):
    for value, label in boundfield.field.choices:
        if str(boundfield.value()) == str(value):
            return label
