import re
from datetime import date

import ldap
from django.conf import settings  # import the settings file
from django.core.management.base import BaseCommand

from lagerregal import utils
from lagerregal.users.models import Department
from lagerregal.users.models import DepartmentUser
from lagerregal.users.models import Lageruser


def iter_ldap_users(page_size=50):
    connection = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
    connection.simple_bind_s(
        settings.AUTH_LDAP_BIND_DN, settings.AUTH_LDAP_BIND_PASSWORD
    )

    q = settings.AUTH_LDAP_USER_SEARCH
    ctrl = ldap.controls.SimplePagedResultsControl(size=page_size, cookie="")

    while True:
        msgid = connection.search_ext(
            q.base_dn,
            q.scope,
            q.filterstr % {"user": "*"},
            serverctrls=[ctrl],
        )
        _, rdata, _, pctrls = connection.result3(msgid)

        yield from settings.AUTH_LDAP_USER_SEARCH._process_results(rdata)

        if pctrls and pctrls[0].cookie:
            ctrl.cookie = pctrls[0].cookie
        else:
            break


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not settings.USE_LDAP:
            self.stdout.write("You have to enable the USE_LDAP setting to use the ldap import.")
            return
        created_users = 0
        updated_users = 0

        for dn, userdata in iter_ldap_users():
            # exclude functional accounts
            if any(attr not in userdata for attr in ["sAMAccountName", "givenName", "sn", "mail"]):
                continue
            username = userdata["sAMAccountName"][0]
            created, user = Lageruser.objects.get_or_create(username=username)

            new_value = userdata.get(settings.AUTH_LDAP_DEPARTMENT_FIELD, [])
            for department_string in new_value:
                department_names = re.findall(settings.AUTH_LDAP_DEPARTMENT_REGEX, department_string)
                for department_name in department_names:
                    _created, department = Department.objects.get_or_create(name=department_name)
                    DepartmentUser.objects.get_or_create(user=user, department=department, defaults={"role": "m"})

            for field, attr in settings.AUTH_LDAP_USER_ATTR_MAP.items():
                if attr in userdata and (created or attr not in settings.AUTH_LDAP_ATTR_NOSYNC):
                    setattr(user, field, userdata[attr][0])

            user.expiration_date = utils.convert_ad_accountexpires(int(userdata["accountExpires"][0]))
            user.is_active = user.expiration_date is None or user.expiration_date > date.today()

            user.save()

            if created:
                created_users += 1
            else:
                updated_users += 1

        if created_users > 0 or updated_users > 0:
            self.stdout.write(f"imported {created_users} new users.")
            self.stdout.write(f"updated {updated_users} exisitng users.")
