import csv
import datetime
import io
import textwrap
import time
import traceback

import pdfrw
from django.conf import settings
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.db.models import Q
from django.db.transaction import atomic
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http.response import HttpResponseNotFound
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import FormView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import View
from django.views.generic.detail import BaseDetailView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from reportlab.graphics.barcode import code128
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from reversion import revisions as reversion
from reversion.models import Version

from lagerregal.devices.forms import VIEWSORTING
from lagerregal.devices.forms import VIEWSORTING_DEVICES
from lagerregal.devices.forms import DeviceForm
from lagerregal.devices.forms import DeviceGroupFilterForm
from lagerregal.devices.forms import DeviceMailForm
from lagerregal.devices.forms import DeviceStorageForm
from lagerregal.devices.forms import DeviceTrashForm
from lagerregal.devices.forms import DeviceViewForm
from lagerregal.devices.forms import FilterForm
from lagerregal.devices.forms import LendForm
from lagerregal.devices.forms import ReturnForm
from lagerregal.devices.forms import ViewForm
from lagerregal.devices.models import Bookmark
from lagerregal.devices.models import Device
from lagerregal.devices.models import Lending
from lagerregal.devices.models import Manufacturer
from lagerregal.devices.models import Note
from lagerregal.devices.models import Picture
from lagerregal.devices.models import Template
from lagerregal.devicetags.forms import DeviceTagForm
from lagerregal.devicetags.models import Devicetag
from lagerregal.devicetypes.models import TypeAttribute
from lagerregal.devicetypes.models import TypeAttributeValue
from lagerregal.mail.helpers import group_and_user_ids_to_email
from lagerregal.mail.helpers import send_automatic_device_update_mail
from lagerregal.mail.helpers import send_automatic_room_update_mail
from lagerregal.mail.helpers import send_device_creation_mail
from lagerregal.mail.helpers import send_device_mail
from lagerregal.mail.models import DeviceUpdateMail
from lagerregal.users.mixins import PermissionRequiredMixin
from lagerregal.users.models import Lageruser
from lagerregal.utils import PaginationMixin

GENERIC_DEVICE_FILTERS_BY_CATEGORY = {
    "all": lambda: Device.objects.all(),
    "available": lambda: Device.active().filter(currentlending=None),
    "lent": lambda: Device.objects.filter(lending__in=Lending.objects.filter(returndate=None)),
    "archived": lambda: Device.objects.exclude(archived=None),
    "trashed": lambda: Device.objects.exclude(trashed=None),
    "overdue": lambda: Device.objects.filter(lending__in=Lending.objects.filter(
        returndate=None,
        duedate__lt=datetime.date.today()
    )),
    "returnsoon": lambda: Device.objects.filter(lending__in=Lending.objects.filter(
        returndate=None,
        duedate__lte=datetime.date.today() + datetime.timedelta(days=10),
        duedate__gt=datetime.date.today()
    )),
    "temporary": lambda: Device.active().filter(templending=True),
    "active": lambda: Device.active(),
}


def get_devices(user, category, department, sorting):
    if category in GENERIC_DEVICE_FILTERS_BY_CATEGORY:
        devices = GENERIC_DEVICE_FILTERS_BY_CATEGORY[category]()
    elif category == "bookmark":
        if not user.is_authenticated:
            raise ValueError(f"Cannot get bookmarked devices for unauthenticated user {user}.")
        devices = user.bookmarks.all()
    else:
        raise ValueError(f"Unrecognized category: {category}")

    if department == "all":
        pass
    elif department == "my":
        devices = devices.filter(department__in=user.departments.all())
    elif department.isdigit():
        devices = devices.filter(department_id=int(department))
    else:
        raise ValueError(f"Department {department} is not a valid department to filter by.")

    if not any(s[0] == sorting for s in VIEWSORTING_DEVICES):
        raise ValueError(f"Unrecognized sorting {sorting}")

    if hasattr(user, "departments"):
        devices = devices.exclude(~Q(department__in=user.departments.all()), is_private=True)
    else:
        devices = devices.exclude(is_private=True)

    return devices.order_by(sorting)


class DeviceList(PermissionRequiredMixin, PaginationMixin, ListView):
    context_object_name = "device_list"
    template_name = "devices/device_list.html"
    viewfilter = None
    viewsorting = None
    permission_required = "devices.view_device"

    def post(self, request):
        """post-requesting the detail-view of device by id"""
        pk = request.POST.get("deviceid")
        if pk and pk.isnumeric():
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": pk}))
        else:
            messages.error(self.request, _("Invalid ID"))
            return HttpResponseRedirect(reverse("device-list"))

    def get_queryset(self):
        """method for query results and display it depending on existing filters (viewfilter, department)"""
        self.viewfilter = self.request.GET.get("category", "active")

        if hasattr(self.request.user, "departments") and self.request.user.departments.count() > 0:
            self.departmentfilter = self.request.GET.get("department", "my")
        else:
            self.departmentfilter = self.request.GET.get("department", "all")

        self.viewsorting = self.request.GET.get("sorting", "name")

        devices = get_devices(self.request.user, self.viewfilter, self.departmentfilter, self.viewsorting)
        return devices.select_related("devicetype", "room__building", "group", "currentlending__owner")

    def get_context_data(self, **kwargs):
        """method for getting context data (filter, time, templates, breadcrumbs)"""
        context = super().get_context_data(**kwargs)

        # getting filters
        context["viewform"] = DeviceViewForm(initial={
            "category": self.viewfilter,
            "sorting": self.viewsorting,
            "department": self.departmentfilter
        })

        context["today"] = datetime.datetime.today()
        context["template_list"] = Template.objects.all()
        context["viewfilter"] = self.viewfilter
        context["breadcrumbs"] = [[reverse("device-list"), _("Devices")]]

        # add page number to breadcrumbs
        if context["is_paginated"] and context["page_obj"].number > 1:
            context["breadcrumbs"].append(["", context["page_obj"].number])
        return context


class ExportCsv(PermissionRequiredMixin, View):
    permission_required = "devices.view_device"
    keys = ["id", "name", "inventorynumber", "devicetype__name", "room__name", "group__name"]
    headers = [_("ID"), _("Device"), _("Inventory Number"), _("Device Type"), _("Room"), _("Device Group")]

    def post(self, request):
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="' + str(int(time.time())) + '_searchresult.csv"'

        devices = get_devices(
            request.user, request.POST["category"], request.POST["department"], request.POST["sorting"]
        )

        writer = csv.writer(response, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(self.headers)
        for device in devices.values_list(*self.keys):
            writer.writerow(device)
        return response


class DeviceDetail(PermissionRequiredMixin, DetailView):
    # get related data to chosen device
    queryset = Device.objects.select_related(
        "manufacturer",
        "devicetype",
        "currentlending",
        "currentlending__owner",
        "department",
        "room",
        "room__building",
    ).prefetch_related("pictures")
    context_object_name = "device"
    object = None
    permission_required = "devices.view_device"
    template_name = "devices/detail/device_detail.html"

    def get_object(self, queryset=None):
        if self.object is not None:
            return self.object

        pk = self.kwargs.get(self.pk_url_kwarg)
        queryset = self.queryset.filter(pk=pk)
        self.object = get_object_or_404(queryset)
        return self.object

    def get_permission_object(self):
        return self.get_object()

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["usedset"] = Device.objects.filter(used_in=self.object)

        # Add in a QuerySet of all the books

        # tag context data
        context["tagform"] = DeviceTagForm()
        context["tagform"].fields["tags"].queryset = Devicetag.objects.exclude(devices=context["device"])

        # lending history, eidt history, mail history
        context["lending_list"] = Lending.objects\
            .filter(device=context["device"])\
            .select_related("owner")\
            .order_by("-pk")[:10]
        context["version_list"] = (
            Version.objects.filter(
                object_id=context["device"].id, content_type_id=ContentType.objects.get(model="device").id
            )
            .select_related("revision", "revision__user")
            .order_by("-pk")[:10]
        )
        context["content_type"] = ContentType.objects.get(model="device").id
        context["mail_list"] = DeviceUpdateMail.objects\
            .filter(device=context["device"])\
            .select_related("sent_by")\
            .order_by("-pk")[:10]

        context["today"] = datetime.date.today()
        context["weekago"] = context["today"] - datetime.timedelta(days=7)
        context["attributevalue_list"] = TypeAttributeValue.objects.filter(device=context["device"])
        context["lendform"] = LendForm()
        mailinitial = {}

        # Pre-fill email modal with lending info if it is currently lent
        if context["device"].currentlending is not None:
            currentowner = context["device"].currentlending.owner
            mailinitial["owner"] = currentowner
            mailinitial["emailrecipients"] = ("u" + str(currentowner.id), currentowner.username)

            if context["device"].is_overdue:
                subject = _('Reminder that device "{0}" is overdue')
                body = 'Device "{0}" is overdue.\n\nIt should have been returned by {1}.\n\nView device: {2}'
            else:
                subject = _('Reminder that device "{0}" is still owned')
                body = 'Device "{0}" is still owned.\n\nIt should be returned by {1}.\n\nView device: {2}'

            mailinitial["emailsubject"] = subject.format(context["device"].name)
            mailinitial["emailbody"] = body.format(
                context["device"].name,
                context["device"].currentlending.duedate,
                self.request.build_absolute_uri(reverse("device-detail", kwargs={"pk": context["device"].pk}))
            )

        # mail context data
        context["mailform"] = DeviceMailForm(initial=mailinitial)
        versions = Version.objects.get_for_object(context["device"])

        context["trashform"] = DeviceTrashForm()

        if len(versions) != 0:
            context["lastedit"] = versions[0]

        # dymo data
        relevant_fields = ["name", "inventorynumber", "serialnumber", "id"]
        dymo_data = {}
        for field_name in relevant_fields:
            value = getattr(self.object, field_name)
            if field_name == "id":
                value = f"{value:07d}"
            dymo_data[field_name] = str(value)
        context["dymo_data"] = dymo_data

        # add data to breadcrumbs
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["device"].pk}), context["device"].name),
        ]

        return context


class DeviceLendingList(PermissionRequiredMixin, PaginationMixin, ListView):
    context_object_name = "lending_list"
    template_name = "devices/device_lending_list.html"
    permission_required = "devices.view_device"

    def get_queryset(self):
        deviceid = self.kwargs["pk"]
        device = get_object_or_404(Device, pk=deviceid)
        return Lending.objects.filter(device=device).order_by("-pk")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["device"] = get_object_or_404(Device, pk=self.kwargs["pk"])
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["device"].pk}), context["device"].name),
            ("", _("Lending")),
        ]
        return context


class DeviceCreate(PermissionRequiredMixin, CreateView):
    model = Device
    template_name = "devices/forms/device_form.html"
    form_class = DeviceForm
    permission_required = "devices.add_device"

    def get_initial(self):
        initial = super().get_initial()
        creator = self.request.user.pk
        copyid = self.kwargs.pop("copyid", None)
        if copyid is not None:
            for key, value in get_object_or_404(Device, pk=copyid).get_as_dict().items():
                initial[key] = value
            initial["deviceid"] = copyid
        initial["creator"] = creator

        # if user has any departments, use one of them as default in form
        first_department = self.request.user.departments.first()
        if first_department:
            initial["department"] = first_department

        return initial

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["form"].fields["department"].queryset = self.request.user.departments.all()
        context["actionstring"] = _("Create new device")
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            ("", _("Create new device"))]
        return context

    def form_valid(self, form):
        if form.cleaned_data["department"]:
            if form.cleaned_data["department"] not in self.request.user.departments.all():
                return HttpResponseBadRequest()
        form.cleaned_data["creator"] = self.request.user
        reversion.set_comment(_("Created"))
        r = super().form_valid(form)
        for key, value in form.cleaned_data.items():
            if key.startswith("attribute_") and value != "":
                attributenumber = key.split("_")[1]
                typeattribute = get_object_or_404(TypeAttribute, pk=attributenumber)
                attribute = TypeAttributeValue()
                attribute.device = self.object
                attribute.typeattribute = typeattribute
                attribute.value = value
                attribute.save()

        if "uses" in form.changed_data:
            for element in form.cleaned_data["uses"]:
                used_device = Device.objects.filter(id=element)[0]
                used_device.used_in = self.object
                used_device.save()

        send_device_creation_mail(self.request, self.object)

        messages.success(self.request, _("Device was successfully created."))
        return r


class DeviceUpdate(PermissionRequiredMixin, UpdateView):
    model = Device
    template_name = "devices/forms/device_form.html"
    form_class = DeviceForm
    permission_required = "devices.change_device"

    def get_permission_object(self):
        return self.get_object()

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["form"].fields["department"].queryset = self.request.user.departments.all()
        context["actionstring"] = _("Update")
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["device"].pk}), context["device"].name),
            ("", _("Edit")),
        ]
        return context

    def update_attributes(self, form, device):
        # If device type changed, delete all associated attributes.
        if form.cleaned_data["devicetype"] is None or form.initial["devicetype"] != form.cleaned_data["devicetype"].pk:
            TypeAttributeValue.objects.filter(device=device.pk).delete()

        # If new device type is None, there is nothing else to do.
        if form.cleaned_data["devicetype"] is None:
            return

        # Add/change type attributes.
        attribute_data = ((key, value) for key, value in form.cleaned_data.items() if key.startswith("attribute_"))
        for key, value in attribute_data:
            attribute_number = key.split("_")[1]
            typeattribute = get_object_or_404(TypeAttribute, pk=attribute_number)

            if not value:
                TypeAttributeValue.objects.filter(device=device.pk, typeattribute=attribute_number).delete()
                continue

            TypeAttributeValue.objects.update_or_create(
                device=device,
                typeattribute=typeattribute,
                defaults={"value": value}
            )

    def update_uses(self, form):
        if "uses" not in form.changed_data:
            return

        previous_uses = {int(element) for element in form.initial["uses"]}
        new_uses = {int(element) for element in form.cleaned_data["uses"]}

        for removed_element in previous_uses - new_uses:
            used_device = Device.objects.filter(id=int(removed_element))[0]
            used_device.used_in = None
            used_device.save()
        for added_element in new_uses - previous_uses:
            used_device = Device.objects.filter(id=int(added_element))[0]
            used_device.used_in = self.object
            used_device.save()

    def send_device_update_mail(self, device, changed_email_fields):
        if not changed_email_fields:
            return

        if "name" in changed_email_fields:
            name_before, name_after = changed_email_fields["name"]
            if len(changed_email_fields) > 1:
                subject = f"Device {name_before} was changed and renamed to {name_after}"
            else:
                subject = f"Device {name_before} was renamed to {name_after}"
        else:
            subject = f"Device {self.object.name} was changed"

        body = subject
        body += "\n\n"

        body += "\n".join(
            f"{field_name.title()}: {value_before} -> {value_after}"
            for field_name, (value_before, value_after) in changed_email_fields.items()
        )

        send_automatic_device_update_mail(self.request, self.object, subject, body)

    def send_update_emails(self, device, changed_email_fields):
        old_room, new_room = changed_email_fields.pop("room", (device.room, device.room))

        self.send_device_update_mail(device, changed_email_fields)
        send_automatic_room_update_mail(self.request, device, old_room, new_room)

    def form_valid(self, form):
        department = form.cleaned_data["department"]
        if department and department not in self.request.user.departments.all():
            return HttpResponseBadRequest()
        device = self.object
        if not device.is_active:
            messages.error(self.request, _("Archived Devices can't be edited"))
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))

        reversion.set_comment(form.cleaned_data["comment"] if form.cleaned_data["comment"] else _("Updated"))

        self.update_attributes(form, device)
        self.update_uses(form)

        messages.success(self.request, _("Device was successfully updated."))

        email_update_fields_before = self.get_object().get_email_update_fields_as_dict()
        result = super().form_valid(form)
        email_update_fields_after = self.object.get_email_update_fields_as_dict()

        changed_email_fields = {
            field_name: (email_update_fields_before[field_name], email_update_fields_after[field_name])
            for field_name in email_update_fields_before
            if email_update_fields_before[field_name] != email_update_fields_after[field_name]
        }

        if changed_email_fields:
            self.send_update_emails(device, changed_email_fields)

        return result


class DeviceDelete(PermissionRequiredMixin, DeleteView):
    model = Device
    success_url = reverse_lazy("device-list")
    template_name = "devices/base_delete.html"
    permission_required = "devices.delete_device"

    def get_permission_object(self):
        return self.get_object()

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["object"].pk}), context["object"].name),
            ("", _("Delete")),
        ]
        return context


class DeviceLend(PermissionRequiredMixin, FormView):
    template_name = "devices/device_lend.html"
    form_class = LendForm
    permission_required = "devices.lend_device"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["actionstring"] = "Mark device as lend"
        if "device" in self.request.POST:
            deviceid = self.request.POST["device"]
            if deviceid != "":
                device = get_object_or_404(Device, pk=deviceid)
                context["breadcrumbs"] = [(reverse("device-list"), _("Devices")), ("", _("Lend"))]
                return context
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            ("", _("Lend"))]
        if self.kwargs and "pk" in self.kwargs:
            device = get_object_or_404(Device, pk=self.kwargs["pk"])
            context["breadcrumbs"] = (
                context["breadcrumbs"][:-1]
                + [(reverse("device-detail", kwargs={"pk": device.pk}), device.name)]
                + context["breadcrumbs"][-1:]
            )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(self.kwargs)
        return kwargs

    def create_device_lending(self, form):
        owner = get_object_or_404(Lageruser, pk=form.cleaned_data["owner"].pk)
        lending = Lending(owner=owner, duedate=form.cleaned_data["duedate"])

        device = form.cleaned_data["device"]

        if not device:
            lending.smalldevice = form.cleaned_data["smalldevice"]
            lending.save()
            return lending

        if not device.is_active:
            messages.error(self.request, _("Archived Devices can't be lent"))
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))

        # End old lending
        if device.currentlending is not None:
            # return current lending first
            oldlending = device.currentlending
            oldlending.returndate = datetime.date.today()
            oldlending.save()

        # Move device to correct room
        new_room = form.cleaned_data["room"]
        if new_room:
            reversion.set_comment(
                _("Device lent to {0} and moved to room {1}").format(form.cleaned_data["owner"], new_room)
            )

            lending_to = f" to {owner.get_full_name()}" if owner else ""
            reason = f"This is the result of a lending of the device{lending_to}\n"
            reason += f"This lending was created by user {self.request.user}."
            send_automatic_room_update_mail(self.request, device, device.room, new_room, reason=reason)

            device.room = form.cleaned_data["room"]
        else:
            reversion.set_comment(_("Device lent to {0}").format(form.cleaned_data["owner"]))

        lending.device = device
        lending.save()

        device.currentlending = lending
        device.save()

        return lending

    def form_valid(self, form):
        lending = self.create_device_lending(form)

        messages.success(
            self.request,
            _("Device was marked as lent to {0}").format(
                get_object_or_404(Lageruser, pk=form.cleaned_data["owner"].pk)
            ),
        )

        if lending.device:
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": lending.device.pk}))
        else:
            return HttpResponseRedirect(reverse("userprofile", kwargs={"pk": lending.owner.pk}))


class DeviceInventoried(PermissionRequiredMixin, View):
    permission_required = "devices.change_device"

    def get(self, request, **kwargs):
        deviceid = kwargs["pk"]
        device = get_object_or_404(Device, pk=deviceid)
        device.inventoried = timezone.now()
        device.save()

        subject = f"Device {device.name} was inventoried and had its current data confirmed"
        send_device_creation_mail(request, device, alternate_subject=subject)

        messages.success(request, _("Device was marked as inventoried."))
        return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))

    def post(self, request, **kwargs):
        return self.get(request, **kwargs)


class DeviceReturn(PermissionRequiredMixin, FormView):
    template_name = "devices/base_form.html"
    form_class = ReturnForm
    permission_required = "devices.lend_device"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["actionstring"] = _("Mark Device as returned")

        # get lending object with given pk
        lending = get_object_or_404(Lending, pk=self.kwargs["lending"])

        if lending.device:
            device_name = lending.device.name
        else:
            device_name = lending.smalldevice
            del context["form"].fields["room"]

        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            ("", _("Return {0}").format(device_name))]
        return context

    def form_valid(self, form):
        device = None
        owner = None
        lending = get_object_or_404(Lending, pk=self.kwargs["lending"])
        if lending.device and lending.device != "":
            device = lending.device
            device.currentlending = None
            new_room = form.cleaned_data["room"]
            if new_room:
                reversion.set_comment(_("Device returned and moved to room {0}").format(new_room))

                lending_to = f" to {lending.owner.get_full_name()}" if lending.owner else ""
                reason = f"This is the result of the device being returned from a lending{lending_to}."
                reason += f"The return of this lending was triggered by user {self.request.user}."
                send_automatic_room_update_mail(self.request, device, device.room, new_room, reason=reason)

                device.room = new_room
            device.save()
        else:
            owner = lending.owner
        lending.returndate = datetime.date.today()
        lending.save()
        messages.success(self.request, _("Device was marked as returned"))
        if device is not None:
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))
        else:
            return HttpResponseRedirect(reverse("userprofile", kwargs={"pk": owner.pk}))


class DeviceMail(PermissionRequiredMixin, FormView):
    template_name = "devices/base_form.html"
    form_class = DeviceMailForm
    permission_required = "devices.lend_device"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Add in a QuerySet of all the books
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": self.device.pk}), self.device.name),
            ("", _("Send Mail")),
        ]
        return context

    def get_initial(self):
        deviceid = self.kwargs["pk"]
        self.device = get_object_or_404(Device, pk=deviceid)
        return {}

    def form_valid(self, form):
        deviceid = self.kwargs["pk"]
        device = get_object_or_404(Device, pk=deviceid)

        subject = form.cleaned_data["emailsubject"]
        body = form.cleaned_data["emailbody"]
        recipients = group_and_user_ids_to_email(form.cleaned_data["emailrecipients"])

        user = self.request.user
        body = f"{user.get_full_name()} (User {user.username}) sent you an email via Lagerregal:\n\n" + body

        try:
            send_device_mail(self.request, device, subject, body, recipients, manual=True)
        except Exception:
            messages.error(self.request, _("Mail could not be sent."))
            traceback.print_exc()

        return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))


class DeviceArchive(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    model = Device
    template_name = "devices/device_archive.html"
    permission_required = "devices.change_device"

    def get_permission_object(self):
        return self.get_object()

    def post(self, request, **kwargs):
        device = self.get_object()
        if device.archived is None:
            device.archive_device()
        else:
            device.archived = None
        device.save()
        reversion.set_comment(_("Device was archived"))
        messages.success(request, _("Device was archived"))
        return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))


class DeviceTrash(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    model = Device
    template_name = "devices/device_trash.html"
    permission_required = "devices.change_device"

    def get_permission_object(self):
        return self.get_object()

    def send_trashed_mail(self, request, device, trashed):
        word = "trashed" if trashed else "untrashed"

        subject = f"Device {device.name} was {word}."
        body = subject

        send_automatic_device_update_mail(self.request, device, subject, body)

    def trash_device(self, request, device, reason):
        device.trash_device(reason)
        device.save()

        self.send_trashed_mail(request, device, True)

        reversion.set_comment(_("Device was trashed"))
        messages.success(request, _("Device was trashed"))

    def untrash_device(self, request, device):
        device.untrash_device()
        device.save()

        self.send_trashed_mail(request, device, False)

        reversion.set_comment(_("Device was untrashed"))
        messages.success(request, _("Device was untrashed."))
        messages.warning(request, _(
            "Note that when trashing the device, properties like its Room and Uses were deleted. "
            "You will have to restore these manually now."
        ))

    def post(self, request, **kwargs):
        device = self.get_object()

        reason = request.POST.get("reason", None)

        if device.trashed is None:
            self.trash_device(request, device, reason)
        else:
            self.untrash_device(request, device)

        return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))


class DeviceStorage(PermissionRequiredMixin, SingleObjectMixin, FormView):
    model = Device
    form_class = DeviceStorageForm
    template_name = "devices/device_storage.html"
    permission_required = "devices.change_device"

    def get_permission_object(self):
        return self.get_object()

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        device = self.get_object()

        if not device.department:
            messages.error(
                self.request, _("Could not move to storage. Device does not have an associated department.")
            )
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))

        storage_room = device.department.storage_room
        if not storage_room:
            messages.error(
                self.request, _(
                    "Could not move to storage. This device's department ({0}) does not currently have a specified "
                    "storage room."
                ).format(device.department.name)
            )
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))

        if storage_room == device.room:
            messages.warning(
                self.request, _("The device was already in storage, so nothing was changed.")
            )
            return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))

        reversion.set_comment(_("Device moved to storage (room {0})").format(storage_room))

        reason = f'This is the result of {self.request.user} clicking the "Move to Storage" button.'
        send_automatic_room_update_mail(self.request, device, device.room, storage_room, reason=reason)

        device.room = storage_room
        device.save()

        messages.success(self.request, _("Device was moved to storage."))
        return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))


class DeviceBookmark(PermissionRequiredMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    model = Device
    permission_required = "devices.view_device"

    def get_permission_object(self):
        return self.get_object()

    def post(self, request, **kwargs):
        device = self.get_object()
        if device.bookmarkers.filter(id=request.user.id).exists():
            bookmark = Bookmark.objects.get(user=request.user, device=device)
            bookmark.delete()
            messages.success(request, _("Bookmark was removed"))
        else:
            bookmark = Bookmark(device=device, user=request.user)
            bookmark.save()
            messages.success(request, _("Device was bookmarked."))
        return HttpResponseRedirect(reverse("device-detail", kwargs={"pk": device.pk}))


class TemplateList(PermissionRequiredMixin, PaginationMixin, ListView):
    model = Template
    context_object_name = "template_list"
    permission_required = "devices.view_template"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("template-list"), _("Templates")),
        ]

        if context["is_paginated"] and context["page_obj"].number > 1:
            context["breadcrumbs"].append(["", context["page_obj"].number])
        return context


class TemplateCreate(PermissionRequiredMixin, CreateView):
    model = Template
    template_name = "devices/base_form.html"
    fields = "__all__"
    permission_required = "devices.add_template"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("template-list"), _("Templates")),
            ("", _("Create new Device Template")),
        ]
        return context


class TemplateUpdate(PermissionRequiredMixin, UpdateView):
    model = Template
    template_name = "devices/base_form.html"
    fields = "__all__"
    permission_required = "devices.change_template"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("template-list"), _("Templates")),
            ("", _("Edit: {0}").format(self.object.templatename)),
        ]
        return context


class TemplateDelete(PermissionRequiredMixin, DeleteView):
    model = Template
    success_url = reverse_lazy("device-list")
    template_name = "devices/base_delete.html"
    permission_required = "devices.delete_template"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("template-list"), _("Templates")),
            ("", _("Delete: {0}").format(self.object.templatename)),
        ]
        return context


class ManufacturerList(PermissionRequiredMixin, PaginationMixin, ListView):
    model = Manufacturer
    context_object_name = "manufacturer_list"
    permission_required = "devices.view_manufacturer"

    def get_queryset(self):
        manufacturers = Manufacturer.objects.all()
        self.filterstring = self.request.GET.get("filter", None)
        if self.filterstring:
            manufacturers = manufacturers.filter(name__icontains=self.filterstring)
        self.viewsorting = self.request.GET.get("sorting", "name")
        if self.viewsorting in [s[0] for s in VIEWSORTING]:
            manufacturers = manufacturers.order_by(self.viewsorting)
        return manufacturers

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [(reverse("manufacturer-list"), _("Manufacturers"))]
        context["viewform"] = ViewForm(initial={"sorting": self.viewsorting})
        if self.filterstring:
            context["filterform"] = FilterForm(initial={"filter": self.filterstring})
        else:
            context["filterform"] = FilterForm()
        if context["is_paginated"] and context["page_obj"].number > 1:
            context["breadcrumbs"].append(["", context["page_obj"].number])
        return context


class ManufacturerDetail(PermissionRequiredMixin, DetailView):
    model = Manufacturer
    context_object_name = "object"
    template_name = "devices/manufacturer_detail.html"
    permission_required = "devices.view_manufacturer"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context["merge_list"] = Manufacturer.objects.exclude(pk=context["object"].pk).order_by("name")
        context["device_list"] = Device.objects.filter(
            manufacturer=context["object"], archived=None, trashed=None
        ).values("id", "name", "inventorynumber", "devicetype__name", "room__name", "room__building__name")

        context["breadcrumbs"] = [
            (reverse("manufacturer-list"), _("Manufacturers")),
            (reverse("manufacturer-detail", kwargs={"pk": context["object"].pk}), context["object"].name),
        ]
        return context


class ManufacturerCreate(PermissionRequiredMixin, CreateView):
    model = Manufacturer
    template_name = "devices/base_form.html"
    fields = "__all__"
    permission_required = "devices.add_manufacturer"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context["actionstring"] = _("Create new Manufacturer")
        context["type"] = "manufacturer"
        context["breadcrumbs"] = [
            (reverse("manufacturer-list"), _("Manufacturers")),
            ("", _("Create new Manufacturer")),
        ]
        return context


class ManufacturerUpdate(PermissionRequiredMixin, UpdateView):
    model = Manufacturer
    template_name = "devices/base_form.html"
    fields = "__all__"
    permission_required = "devices.change_manufacturer"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context["actionstring"] = _("Update")
        context["breadcrumbs"] = [
            (reverse("manufacturer-list"), _("Manufacturers")),
            (reverse("manufacturer-detail", kwargs={"pk": context["object"].pk}), context["object"].name),
            ("", _("Edit")),
        ]
        return context


class ManufacturerDelete(PermissionRequiredMixin, DeleteView):
    model = Manufacturer
    success_url = reverse_lazy("manufacturer-list")
    template_name = "devices/base_delete.html"
    permission_required = "devices.delete_manufacturer"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("manufacturer-list"), _("Manufacturers")),
            (reverse("manufacturer-detail", kwargs={"pk": context["object"].pk}), context["object"].name),
            ("", _("Delete")),
        ]
        return context


class ManufacturerMerge(PermissionRequiredMixin, View):
    model = Manufacturer
    permission_required = "devices.change_manufacturer"

    def get(self, request, *args, **kwargs):
        context = {
            "oldobject": get_object_or_404(self.model, pk=kwargs["oldpk"]),
            "newobject": get_object_or_404(self.model, pk=kwargs["newpk"]),
        }
        context["breadcrumbs"] = [
            (reverse("manufacturer-list"), _("Manufacturers")),
            (reverse("manufacturer-detail", kwargs={"pk": context["oldobject"].pk}), context["oldobject"].name),
            ("", _("Merge with {0}").format(context["newobject"].name)),
        ]
        return render(request, "devices/base_merge.html", context)

    @atomic
    def post(self, request, *args, **kwargs):
        oldobject = get_object_or_404(self.model, pk=kwargs["oldpk"])
        newobject = get_object_or_404(self.model, pk=kwargs["newpk"])
        devices = Device.objects.filter(manufacturer=oldobject)
        for device in devices:
            device.manufacturer = newobject
            reversion.set_comment(_("Merged Manufacturer {0} into {1}").format(oldobject, newobject))
            device.save()
        oldobject.delete()
        return HttpResponseRedirect(newobject.get_absolute_url())


class NoteCreate(CreateView):
    model = Note
    template_name = "devices/base_form.html"
    fields = "__all__"

    def get_initial(self):
        initial = super().get_initial()
        initial["device"] = get_object_or_404(Device, pk=self.kwargs["pk"])
        initial["creator"] = self.request.user
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        device = get_object_or_404(Device, pk=self.kwargs["pk"])
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": device.pk}), device),
            ("", _("Notes")),
            ("", _("Create new note")),
        ]
        return context


class NoteUpdate(UpdateView):
    model = Note
    template_name = "devices/base_form.html"
    fields = "__all__"

    def get_success_url(self):
        return reverse_lazy("device-detail", kwargs={"pk": self.object.device.pk})

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["object"].device.pk}), context["object"].device.name),
            ("", _("Edit")),
        ]
        return context


class NoteDelete(DeleteView):
    model = Note
    template_name = "devices/base_delete.html"

    def get_success_url(self):
        return reverse_lazy("device-detail", kwargs={"pk": self.object.device.pk})

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["object"].device.pk}), context["object"].device.name),
            ("", _("Delete")),
        ]
        return context


class PictureCreate(CreateView):
    model = Picture
    template_name = "devices/base_form.html"
    fields = "__all__"

    def get_initial(self):
        initial = super().get_initial()
        initial["device"] = get_object_or_404(Device, pk=self.kwargs["pk"])
        initial["creator"] = self.request.user
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        device = get_object_or_404(Device, pk=self.kwargs["pk"])
        context["enctype"] = "multipart/form-data"
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": device.pk}), device),
            ("", _("Notes")),
            ("", _("Create new note")),
        ]
        return context


class PictureUpdate(UpdateView):
    model = Picture
    template_name = "devices/base_form.html"
    fields = "__all__"

    def get_success_url(self):
        return reverse_lazy("device-detail", kwargs={"pk": self.object.device.pk})

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["enctype"] = "multipart/form-data"
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["object"].device.pk}), context["object"].device.name),
            ("", _("Edit")),
        ]
        return context


class PictureDelete(DeleteView):
    model = Picture
    template_name = "devices/base_delete.html"

    def get_success_url(self):
        return reverse_lazy("device-detail", kwargs={"pk": self.object.device.pk})

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("device-list"), _("Devices")),
            (reverse("device-detail", kwargs={"pk": context["object"].device.pk}), context["object"].device.name),
            ("", _("Delete")),
        ]
        return context


class Search(PermissionRequiredMixin, ListView):
    model = Device
    template_name = "devices/search.html"
    permission_required = "devices.view_device"

    STRING_FIELDS = {
        "name": "name",
        "inventorynumber": "inventorynumber",
        "serialnumber": "serialnumber",
        "hostname": "hostname",
        "description": "description",
        "manufacturer": "manufacturer__name",
        "room": "room__name",
        "building": "room__building__name",
        "type": "devicetype__name",
        "group": "group__name",
        "contact": "contact__username",
        "department": "department__name",
        "tag": "tags__name",
        "user": "currentlending__owner__username",
    }
    DATE_FIELDS = {
        "archived": "archived",
        "trashed": "trashed",
        "inventoried": "inventoried",
    }

    def get_searchstring(self):
        return self.request.GET.get("searchstring", "")

    def parse_searchstring(self, searchstring):
        """
        Example:

            `foo "foo bar" baz:2`
            [(None, 'foo'), (None, 'foo bar'), ('baz', '2')]
        """

        in_string = False
        key = ""
        token = ""
        for c in searchstring + " ":
            if in_string:
                if c == '"':
                    in_string = False
                else:
                    token += c
            else:
                if c == '"':
                    in_string = True
                elif c == " ":
                    if key:
                        yield key, token
                        key = ""
                        token = ""
                    elif token:
                        yield None, token
                        token = ""
                elif c == ":" and not key:
                    key = token
                    token = ""
                else:
                    token += c

    def get_queryset(self):
        searchstring = self.get_searchstring()

        if not searchstring:
            return Device.objects.none()

        qs = Device.objects.all()

        for key, value in self.parse_searchstring(searchstring):
            if key is None:
                q = models.Q()
                for k in self.STRING_FIELDS.values():
                    q |= models.Q(**{f"{k}__icontains": value})
                qs = qs.filter(q)
            elif key in self.STRING_FIELDS:
                k = self.STRING_FIELDS[key]
                qs = qs.filter(**{f"{k}__icontains": value})
            elif key in self.DATE_FIELDS:
                k = self.DATE_FIELDS[key]
                parsed = value.lower() in ["", "0", "false", "no"]
                qs = qs.filter(**{f"{k}__isnull": parsed})
            else:
                raise SuspiciousOperation(_("Invalid search"))

        return qs

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [("", _("Search"))]
        context["searchstring"] = self.get_searchstring()
        context["keys"] = sorted(
            set(list(self.STRING_FIELDS.keys()) + list(self.DATE_FIELDS.keys()))
        )
        return context


class PublicDeviceListView(ListView):
    filterstring = ""
    groupfilter = None
    viewsorting = None
    template_name = "devices/public_devices_list.html"

    def get_queryset(self):
        query_dict = settings.PUBLIC_DEVICES_FILTER
        if len(query_dict) == 0:
            raise ImproperlyConfigured

        devices = Device.objects.filter(**query_dict)
        self.filterstring = self.request.GET.get("filter", None)
        if self.filterstring:
            devices = devices.filter(name__icontains=self.filterstring)
        self.viewsorting = self.request.GET.get("sorting", "name")
        if self.viewsorting in [s[0] for s in VIEWSORTING]:
            devices = devices.order_by(self.viewsorting)
        self.groupfilter = self.request.GET.get("group", "all")
        if self.groupfilter != "all":
            devices = devices.filter(group__id=self.groupfilter)
        return devices.values(
            "id",
            "name",
            "inventorynumber",
            "devicetype__name",
            "room__name",
            "room__building__name",
            "group__name",
            "currentlending__owner__username",
            "currentlending__duedate",
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [(reverse("public-device-list"), _("Public Devices"))]
        context["viewform"] = ViewForm(initial={"sorting": self.viewsorting})
        if self.filterstring:
            context["filterform"] = FilterForm(initial={"filterstring": self.filterstring})
        else:
            context["filterform"] = FilterForm()
        context["groupfilterform"] = DeviceGroupFilterForm(initial={"group": self.groupfilter})
        if context["is_paginated"] and context["page_obj"].number > 1:
            context["breadcrumbs"].append(["", context["page_obj"].number])
        context["nochrome"] = self.request.GET.get("nochrome", False)
        return context


class PublicDeviceDetailView(DetailView):
    template_name = "devices/detail/device_detail.html"
    context_object_name = "device"

    def get_queryset(self):
        query_dict = settings.PUBLIC_DEVICES_FILTER
        if len(query_dict) == 0:
            raise ImproperlyConfigured

        devices = Device.objects.prefetch_related("room", "room__building", "manufacturer", "devicetype").filter(
            **query_dict
        )
        devices = devices.filter(id=self.kwargs.get("pk", None))
        if devices.count() != 1:
            raise Http404

        return devices

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("public-device-list"), _("Public Devices")),
            (reverse("public-device-detail", kwargs={"pk": context["device"].pk}), context["device"].name),
        ]
        context["nochrome"] = self.request.GET.get("nochrome", False)
        context["content_type"] = ContentType.objects.get(model="device").id
        return context


def generate_label_pdf(request, pk):
    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()
    if hasattr(settings, "LABEL_PAGESIZE"):
        size = settings.LABEL_PAGESIZE
    else:
        size = A4
    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas(buffer, pagesize=size)

    offset = 0
    if hasattr(settings, "LABEL_ICON"):
        icon_size = size[1] * 0.9
        offset = size[1] * 0.05
        p.drawInlineImage(settings.LABEL_ICON, offset, offset, icon_size, icon_size)
        size = (size[0] - icon_size, size[1])
        offset += icon_size + offset

    barcode = code128.Code128(f"{pk:06d}", barHeight=size[1] / 2, barWidth=2)
    barcode._calculate()
    width, height = barcode._width, barcode._height
    barcode.drawOn(p, offset + (size[0] - width) / 2, (size[1] - height) / 2)
    p.drawCentredString(offset + (size[0] / 2) + 1, (size[1] - height) / 2 - 11, f"{pk:06d}")
    if hasattr(settings, "LABEL_TITLE"):
        p.setFontSize(9)
        p.drawCentredString(offset + (size[0] / 2) + 1, (size[1] - height) / 2 + height + 6, settings.LABEL_TITLE)

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    response = HttpResponse(buffer, content_type="application/pdf")
    response["Content-Disposition"] = f"filename=label_{pk}.pdf"
    return response


def merge(overlay_canvas: io.BytesIO, template_path: str) -> io.BytesIO:
    template_pdf = pdfrw.PdfReader(template_path)
    overlay_pdf = pdfrw.PdfReader(overlay_canvas)
    for page, data in zip(template_pdf.pages, overlay_pdf.pages):
        overlay = pdfrw.PageMerge().add(data)[0]
        pdfrw.PageMerge(page).add(overlay).render()
    form = io.BytesIO()
    pdfrw.PdfWriter().write(form, template_pdf)
    form.seek(0)
    return form


def get_protocol_data_for_purpose(purpose):
    if purpose == "handover" and hasattr(settings, "HANDOVER_PROTOCOL_LOCATION"):
        return settings.HANDOVER_PROTOCOL_LOCATION, settings.HANDOVER_PROTOCOL_TEXT_LOCATIONS
    if purpose == "trashed" and hasattr(settings, "TRASHED_PROTOCOL_LOCATION"):
        return settings.TRASHED_PROTOCOL_LOCATION, settings.TRASHED_PROTOCOL_TEXT_LOCATIONS
    if purpose == "returned" and hasattr(settings, "RETURNED_PROTOCOL_LOCATION"):
        return settings.RETURNED_PROTOCOL_LOCATION, settings.RETURNED_PROTOCOL_TEXT_LOCATIONS
    return None, None


def generate_device_protocol(request, pk, purpose):
    device = get_object_or_404(Device, pk=pk)

    file_location, text_locations = get_protocol_data_for_purpose(purpose)
    if not file_location or not text_locations:
        return HttpResponseNotFound()

    data = io.BytesIO()
    pdf = canvas.Canvas(data)
    data_map = {
        "user_name": str(request.user),
        "date": str(datetime.date.today()),
        "devicetype": str(device.devicetype),
        "manufacturer": str(device.manufacturer),
        "inventorynumber": device.inventorynumber,
        "serialnumber": device.serialnumber,
        "name": device.name,
        "room": str(device.room),
        "department": str(device.department),
    }

    if purpose == "trashed":
        if not device.trashed:
            return HttpResponseBadRequest()
        data_map["comment"] = device.trashed_reason if device.trashed_reason else ""

    if device.currentlending is not None:
        data_map["recipient_name"] = str(device.currentlending.owner)

    wrapper = textwrap.TextWrapper()
    for key, location in text_locations.items():
        if key not in data_map:
            continue

        text = data_map[key]
        if len(location) == 3:
            wrapper.width = location[2]
        else:
            wrapper.width = 70
        lines = wrapper.wrap(text)
        for index, line in enumerate(lines, start=0):
            pdf.drawString(x=location[0], y=location[1] - (index * (pdf._fontsize + 4)), text=line)

    for checkmark_location in text_locations.get("checkmarks", []):
        pdf.drawString(x=checkmark_location[0], y=checkmark_location[1], text="✔")

    pdf.save()
    data.seek(0)

    buffer = merge(data, template_path=file_location)
    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    response = HttpResponse(buffer, content_type="application/pdf")
    response["Content-Disposition"] = f"filename={purpose}_protocol_{pk}.pdf"
    return response
