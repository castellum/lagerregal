from django.contrib import admin

from lagerregal.devices.models import Device
from lagerregal.devices.models import DeviceInformation
from lagerregal.devices.models import DeviceInformationType
from lagerregal.devices.models import Lending
from lagerregal.devices.models import Manufacturer
from lagerregal.devices.models import Note
from lagerregal.devices.models import Picture
from lagerregal.devices.models import Template
from lagerregal.locations.models import Building
from lagerregal.locations.models import Room


class DeviceAdmin(admin.ModelAdmin):
    # List display
    list_display = ("name",)


admin.site.register(Building)
admin.site.register(Room)
admin.site.register(Manufacturer)
admin.site.register(Device, DeviceAdmin)
admin.site.register(DeviceInformationType)
admin.site.register(DeviceInformation)
admin.site.register(Lending)
admin.site.register(Template)
admin.site.register(Note)
admin.site.register(Picture)
