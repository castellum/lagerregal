from django.forms.models import modelform_factory
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import QueryDict
from django.shortcuts import get_object_or_404
from django.template import defaultfilters
from django.template.loader import render_to_string
from django.views.generic.base import View

from lagerregal.devicegroups.models import Devicegroup
from lagerregal.devices.forms import AddForm
from lagerregal.devices.models import Manufacturer
from lagerregal.devicetypes.models import Type
from lagerregal.locations.models import Room
from lagerregal.users.models import Lageruser

FIELD_NAME_TO_CLASS = {
    "manufacturer": Manufacturer,
    "devicetype": Type,
    "room": Room,
    "group": Devicegroup,
}


class AddDeviceField(View):
    def post(self, request):
        dform = QueryDict(query_string=str(request.POST["form"]).encode("utf-8"))

        classname = dform["classname"]
        desired_class = FIELD_NAME_TO_CLASS.get(classname)
        if not desired_class:
            return HttpResponse("")

        form = modelform_factory(desired_class, exclude=(), form=AddForm)(dform)

        data = {}
        if form.is_valid():
            if request.user.is_staff:
                new_item = form.save()

                data["id"] = new_item.pk
                data["name"] = new_item.name
                data["classname"] = classname
            else:
                data["error"] = "Missing permissions."
        else:
            data["error"] = f"Error: {form.non_field_errors()}"
        return JsonResponse(data)


class LoadExtraform(View):
    def get(self, request):
        classname = request.GET["classname"]
        desired_class = FIELD_NAME_TO_CLASS.get(classname)
        if not desired_class:
            return HttpResponse("")

        form = modelform_factory(desired_class, exclude=(), form=AddForm)()
        return HttpResponse(render_to_string("snippets/formfields.html", {"form": form}))


class UserLendings(View):
    def get(self, request):
        user = request.GET["user"]
        if user == "":
            return HttpResponse("")
        user = get_object_or_404(Lageruser, pk=user)
        data = {}
        data["devices"] = [
            [
                lending.device.name if lending.device else lending.smalldevice,
                lending.device.inventorynumber if lending.device else "",
                lending.device.serialnumber if lending.device else "",
                defaultfilters.date(lending.duedate, "SHORT_DATE_FORMAT") if lending.duedate else "",
                lending.pk,
                lending.is_overdue,
            ]
            for lending in user.lending_set.filter(returndate=None)
        ]

        return JsonResponse(data)
