from django.http import JsonResponse
from django.views.generic.base import View

from lagerregal.devicetypes.models import TypeAttribute


class GetTypeAttributes(View):
    def get(self, request):
        pk = request.GET["pk"]

        attributes = TypeAttribute.objects.filter(devicetype__pk=pk)
        data = []

        for attribute in attributes:
            data.append({
                "id": attribute.pk,
                "name": attribute.name,
            })

        return JsonResponse({"attributes": data})
