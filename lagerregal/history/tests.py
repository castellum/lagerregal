from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.test.client import Client
from model_bakery import baker

from lagerregal.devices.models import Device
from lagerregal.users.models import Lageruser


class HistoryTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.admin = Lageruser.objects.create_superuser("test", "test@test.com", "test")
        self.client.login(username="test", password="test")

    def test_global_view(self):
        response = self.client.get("/history/global/")
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        content_type = ContentType.objects.get(model="device")
        device = baker.make(Device)
        response = self.client.get(f"/history/{content_type.pk}/{device.pk}/")
        self.assertEqual(response.status_code, 200)

    def test_detail_view(self):
        device = baker.make(Device)
        response = self.client.post(f"/devices/{device.pk}/edit/", data={
            "name": "test",
            "creator": self.admin.pk,
        })
        self.assertEqual(response.status_code, 302)
        response = self.client.get("/history/version/1/")
        self.assertEqual(response.status_code, 200)
