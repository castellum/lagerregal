from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from model_bakery import baker

from lagerregal.locations.models import Section
from lagerregal.users.models import Lageruser


class SectionTests(TestCase):
    def setUp(self):
        self.client = Client()
        Lageruser.objects.create_superuser("test", "test@test.com", "test")
        self.client.login(username="test", password="test")

    def test_section_creation(self):
        section = baker.make(Section)
        self.assertEqual(str(section), section.name)
        self.assertEqual(section.get_absolute_url(), reverse("section-detail", kwargs={"pk": section.pk}))
        self.assertEqual(section.get_edit_url(), reverse("section-edit", kwargs={"pk": section.pk}))

    def test_list_view(self):
        response = self.client.get("/sections/")
        self.assertEqual(response.status_code, 200)

    def test_create_view(self):
        response = self.client.get("/sections/add/")
        self.assertEqual(response.status_code, 200)

    def test_detail_view(self):
        section = baker.make(Section)
        response = self.client.get(f"/sections/{section.pk}/")
        self.assertEqual(response.status_code, 200)

    def test_update_view(self):
        section = baker.make(Section)
        response = self.client.get(f"/sections/{section.pk}/edit/")
        self.assertEqual(response.status_code, 200)

    def test_delete_view(self):
        section = baker.make(Section)
        response = self.client.get(f"/sections/{section.pk}/delete/")
        self.assertEqual(response.status_code, 200)

    def test_merge_view(self):
        section1 = baker.make(Section)
        section2 = baker.make(Section)
        response = self.client.get(f"/sections/{section1.pk}/merge/{section2.pk}/")
        self.assertEqual(response.status_code, 200)
