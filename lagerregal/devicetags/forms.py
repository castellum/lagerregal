from django import forms

from lagerregal.devicetags.models import Devicetag
from lagerregal.utils import Select2MultipleWidget


class TagForm(forms.ModelForm):
    class Meta:
        model = Devicetag
        exclude = ["devices"]


class DeviceTagForm(forms.Form):
    error_css_class = "has-error"
    tags = forms.ModelMultipleChoiceField(
        Devicetag.objects.all(),
        widget=Select2MultipleWidget(attrs={"data-token-separators": '[",", " "]'}),
    )
