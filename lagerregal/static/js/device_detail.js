$(function () {
    $(document).on("click", ".delete-picture", function (e) {
        const picture_id = e.currentTarget.id.replace("picture", "");
        const device_id = $("#manual_details_header").data("deviceid");
        const url = `/api/devices/${device_id}/pictures/${picture_id}/`;
        $.ajax({
            url: url,
            type: "DELETE",
            success: function () {
                var row = e.currentTarget.parentNode.parentNode;
                var modalBody = row.parentNode;
                modalBody.removeChild(row);
            },
        });
    });

    $(document).on("click", ".rotate-picture-right", function (e) {
        rotate_and_reload(this, "right");
    });
    $(document).on("click", ".rotate-picture-left", function (e) {
        rotate_and_reload(this, "left");
    });

    function rotate_and_reload(target, direction) {
        const picture_id = target.id.replace("picture", "");
        const device_id = $("#manual_details_header").data("deviceid");
        const url = `/api/devices/${device_id}/pictures/${picture_id}/rotate/${direction}/`;
        $.ajax({
            url: url,
            type: "PATCH",
            success: function (data) {
                var d = new Date();
                img = target.parentElement.parentElement.getElementsByTagName("img")[0];
                if (data.new_source != data.old_source) {
                    // image was converted to png
                    img.src = img.src.replace(data.old_source, data.new_source);
                } else {
                    // force cache invalidation and reload
                    img.src = img.src.split("?")[0] + "?" + d.getTime();
                }
            },
        });
    }
});
