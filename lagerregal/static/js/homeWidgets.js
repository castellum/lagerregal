$(function () {
    $body = $("body");

    $(".dashboard").sortable({
        connectWith: ".dashboard",
        placeholder: "alert alert-success dashboard-placeholder",
        distance: 15,
        update: function (event, ui) {
            if (this !== ui.item.parent()[0]) {
                return;
            }

            widgets = {};
            $(".dashboard-left")
                .children(".widget")
                .each(function (i) {
                    widgets[$(this).attr("id")] = {
                        index: $(this).index() + 1,
                        column: "l",
                    };
                });
            $(".dashboard-right")
                .children(".widget")
                .each(function (i) {
                    widgets[$(this).attr("id")] = {
                        index: $(this).index() + 1,
                        column: "r",
                    };
                });
            $.ajax({
                type: "POST",
                url: "/devices_ajax/move_widget/",
                data: { widgets: JSON.stringify(widgets) },
            });
        },
    });

    $body.on("click", ".card-title .w-minimize", function () {
        $.ajax({
            type: "POST",
            url: "/devices_ajax/toggle_widget/",
            context: $(this).parents(".widget:first"),
            data: { widgetname: $(this).parents(".widget:first").attr("id") },
        }).done(function (data) {
            $(this).find(".w-minimize").toggleClass("fa-minus").toggleClass("fa-plus");
            $(this).find(".card-header").next().slideToggle("fast");
        });
    });

    $body.on("click", ".card-title .w-close", function () {
        $.ajax({
            type: "POST",
            url: "/devices_ajax/remove_widget/",
            context: $(this).parents(".widget:first"),
            data: { widgetname: $(this).parents(".widget:first").attr("id") },
        }).done(function (data) {
            $(this).slideUp("fast", function () {
                $(this).remove();
            });
            $("#widgetlist").append(
                '<li><a href="#" data-name="' +
                    $(this).attr("id") +
                    '" class="addWidget">' +
                    $(this).find(".widget-title").text() +
                    "</a></li>",
            );
        });
    });

    $body.on("click", ".addWidget", function () {
        $.ajax({
            type: "POST",
            context: $(this),
            url: "/devices_ajax/add_widget/",
            data: { widgetname: $(this).data("name") },
        }).done(function (data) {
            $(this).parent().remove();
            $(".dashboard-left").append(data);
            $("#addWidgetModal").modal("hide");
        });
    });

    $(".card-title .pull-right").hide();

    $body.on(
        {
            mouseenter: function () {
                $(this).find(".pull-right").fadeIn();
            },
            mouseleave: function () {
                $(this).find(".pull-right").fadeOut();
            },
        },
        ".card-header",
    );

    $(".dashboard").disableSelection();
});
