$("#return_userlist").on("select2:select", function (e) {
    $("#listwrapper").html('<table id="lendinglist"class="table table-bordered table-striped"></table>');
    $.ajax({
        context: $(this),
        url: "/devices_ajax/user_lendings/",
        data: { user: e.params.data.id },
    }).done(function (data) {
        $("#lendinglist").dataTable({
            aaData: data["devices"],
            aoColumns: [
                { sTitle: django.gettext("Device") },
                { sTitle: django.gettext("Inventory Number") },
                { sTitle: django.gettext("Serial Number") },
                {
                    sTitle: django.gettext("Due date"),
                    mRender: function (data, type, full) {
                        if (full[5]) {
                            var span = document.createElement("span");
                            span.className = "text-danger";
                            span.title = django.gettext("overdue");
                            span.textContent = data;
                            return span.outerHTML;
                        } else {
                            return data;
                        }
                    },
                },
                {
                    orderable: false,
                    mRender: function (data) {
                        var button = document.createElement("a");
                        button.className = "btn btn-secondary btn-sm";
                        button.textContent = django.gettext("Return");
                        button.href = `/devices/return/${data}/`;
                        return button.outerHTML;
                    },
                },
            ],
            bLengthChange: false,
            bFilter: false,
        });
    });
});
