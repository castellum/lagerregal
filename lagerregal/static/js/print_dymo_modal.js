function updatePreview(label) {
    if (!label) return;

    const pngData = label.render();
    $("#labelImage").attr("src", "data:image/png;base64," + pngData);
}

function loadPrinters() {
    const printersSelect = $("#printersSelect");
    printersSelect.empty();

    const printers = dymo.label.framework.getPrinters();
    if (printers.length === 0) {
        const option = document.createElement("option");
        option.appendChild(document.createTextNode("No DYMO-Printers found"));
        printersSelect.append(option);
        $("#printButton").disabled = true;
        return;
    }

    for (let i = 0; i < printers.length; i++) {
        const printer = printers[i];
        if (printer.printerType === "LabelWriterPrinter") {
            const printerName = printer.name;
            const option = document.createElement("option");
            option.value = printerName;
            option.appendChild(document.createTextNode(printerName));
            printersSelect.append(option);
        }
        printersSelect.val(printersSelect.children("option:eq(0)").val());
    }
}

// New Dymo version hack: https://github.com/dymosoftware/dymo-connect-framework/issues/10
dymo.xml.serialize = function (e) {
    function fix(e) {
        return e.replaceAll(/<Color([^\/]+)?\/>/g, "<Color$1> </Color>");
    }

    return fix(goog.dom.xml.serialize(e));
};

$("#printDymoModal").on("show.bs.modal", function () {
    loadPrinters();

    const labelUri = window.location.protocol + "//" + window.location.host + "/static/labels/device_all_deps.dymo";
    const label = dymo.label.framework.openLabelFile(labelUri);

    const label_data_json = document.getElementById("dymo_data").textContent;
    const label_data = JSON.parse(label_data_json);
    for (const key in label_data) {
        value = label_data[key];
        label.setObjectText(key, value);
    }

    updatePreview(label);

    $("#printButton").click(function () {
        try {
            label.print($("#printersSelect").val());
        } catch (e) {
            alert(e.message || e);
        } finally {
            $("#printDymoModal").modal("hide");
        }
    });
});
