// Solving this dropdown display issue: https://select2.org/troubleshooting/common-problems
$(".select2Widget").each((index, element) => {
    $(element).select2({
        dropdownParent: $(element).closest(".modal, body"),
    });
});
