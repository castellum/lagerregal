// When the device type is changed, load in the new device type's device attributes.
$("#id_devicetype").change(function () {
    $.ajax({
        url: "/devices_ajax/get_attributes/",
        data: { pk: $(this).val() },
    }).done(function (data) {
        $("#extra_attributes").find(".row").hide();
        $("#extra_attributes").find("input").attr("disabled", true);

        if (data !== "") {
            $.each(data.attributes, function (index, value) {
                var $input = $(`#id_attribute_${value.id}`);
                if ($input.length) {
                    $input.parent().parent().show();
                    $input.attr("disabled", false);
                } else {
                    $("#extra_attributes").append(`
                        <div class="mb-3 row">
                            <label for="id_attribute_${value.id}" class="col-lg-3 control-label">${value.name}</label>
                            <div class="col-lg-9">
                                <input type="text" id="id_attribute_${value.id}" name="attribute_${value.id}" class="form-control"/>
                            </div>
                        </div>
                    `);
                }
            });
        }
    });
});

// When a modal-opening button is clicked, request its associated form from the Django Form class, and add it to the modal.
$(".modalbutton").click(function () {
    $("#addmodal_header").text($(this).data("name"));
    $("#addmodal_classname").val($(this).data("classname"));
    $.ajax({
        url: "/devices_ajax/load_extraform/",
        data: { classname: $(this).data("classname") },
    }).done(function (data) {
        $("#modal-form").html(data);
        $("#addModal").modal("show");
    });
});

$("#addform").on("submit", function (event) {
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: "/devices_ajax/add_device_field/",
        data: { form: $("#addform").serialize(true) },
    }).done(function (data) {
        if (data.error) {
            $("#addform").append("already exists");
        } else {
            $("#id_" + data.classname).append('<option value="' + data.id + '">' + data.name + "</option>");
            $("#id_" + data.classname).val(data.id);
            $("#addModal").modal("hide");
        }
    });
});

// Show "is private" button when a department is selected.
function showIsPrivateDevice(show) {
    $("#id_is_private").prop("disabled", !show);
}

$("#id_department").change(function () {
    showIsPrivateDevice($(this).val() !== "");
});

// Depending on how the device form was accessed ("add" vs "edit"), a Deparment may or may not already be selected.
showIsPrivateDevice($("#id_department").val() !== "");

// If the user selected any email recipients, then the submit should not immediately go through.
// Instead, we ask the user if they are sure they want to send the email.
$("#form").submit(function (e) {
    if ($("#id_emailrecipients").val().length !== 0 && !$("#mailSendConfirm").is(":visible")) {
        e.preventDefault();
        $("#mailSendConfirm").modal("show");
    }
});
