from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView

from lagerregal.devices.forms import LendForm
from lagerregal.devices.models import Device
from lagerregal.main.models import DashboardWidget
from lagerregal.main.widgets import WIDGET_NAMES
from lagerregal.main.widgets import WIDGETS
from lagerregal.users.models import Lageruser


class Home(TemplateView):
    template_name = "home.html"

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated and not self.request.user.is_staff:
            return redirect("userprofile")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not self.request.user.is_staff:
            return context

        context["widgets_left"] = (
            DashboardWidget.objects
            .filter(user=self.request.user, column="l")
            .order_by("index")
        )
        context["widgets_right"] = (
            DashboardWidget.objects
            .filter(user=self.request.user, column="r")
            .order_by("index")
        )
        userwidget_list = dict(WIDGET_NAMES)
        widgetlist = [
            x[0] for x in DashboardWidget.objects.filter(user=self.request.user).values_list("widgetname")
        ]

        for widget in widgetlist:
            context.update(WIDGETS[widget].get_context_data(self.request.user, only_user=True))

        for w in context["widgets_left"]:
            if w.widgetname in userwidget_list:
                del userwidget_list[w.widgetname]
            else:
                w.delete()

        for w in context["widgets_right"]:
            if w.widgetname in userwidget_list:
                del userwidget_list[w.widgetname]
            else:
                w.delete()
        context["widgets_list"] = userwidget_list
        context["lendform"] = LendForm()
        context["lendform"].fields["device"].choices = [
            [device[0], str(device[0]) + " - " + device[1]] for device in (
                Device
                .devices_for_departments(self.request.user.departments.all())
                .filter(trashed=None, currentlending=None, archived=None)
                .values_list("id", "name")
            )
        ]
        context["lendform"].fields["device"].choices.insert(0, ["", "---------"])
        context["userlist"] = Lageruser.objects.filter(is_active=True).values(
            "pk", "username", "first_name", "last_name"
        )
        context["breadcrumbs"] = [("", _("Dashboard"))]

        return context
