from django.conf import settings
from django.contrib.auth.models import Permission
from django.test import TestCase
from django.test.client import Client
from model_bakery import baker

from lagerregal.devices.models import Device
from lagerregal.devicetags.models import Devicetag
from lagerregal.main.widgets import get_progresscolor
from lagerregal.users.models import Department
from lagerregal.users.models import Lageruser


class TestMethods(TestCase):
    def setUp(self):
        self.client = Client()
        Lageruser.objects.create_superuser("test", "test@test.com", "test")
        self.client.login(username="test", password="test")

    def test_progresscolor(self):
        self.assertEqual(get_progresscolor(91), "danger")
        self.assertEqual(get_progresscolor(90), "warning")
        self.assertEqual(get_progresscolor(59), "success")

    def test_home_view(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)


class TestDevicePerms(TestCase):
    def setUp(self):
        department = baker.make(Department)

        self.device = baker.make(Device, department=department)
        self.private_device = baker.make(Device, department=department, is_private=True)

        devicetag = baker.make(Devicetag)
        settings.PUBLIC_DEVICES_FILTER = {"tags__id__in": [devicetag.id]}

        self.public_device = baker.make(Device, department=department, tags=[devicetag])

        view_device_permission = Permission.objects.get(codename="view_device")
        change_device_permission = Permission.objects.get(codename="change_device")
        full_permissions = [view_device_permission, change_device_permission]

        self.weak_user = baker.make(Lageruser)
        self.strong_user = baker.make(Lageruser, user_permissions=full_permissions)
        self.weak_user_department = baker.make(Lageruser, departments=[department])
        self.strong_user_department = baker.make(
            Lageruser, departments=[department], user_permissions=full_permissions
        )

    def test_device_access(self):
        """
        Viewing a device that is neither explicitly private nor public requires exactly the view_device permission.
        Changing such a device also requires being part of the same department.
        """
        self.assertFalse(self.weak_user.has_perm("devices.view_device", self.device))
        self.assertFalse(self.weak_user_department.has_perm("devices.view_device", self.device))
        self.assertTrue(self.strong_user.has_perm("devices.view_device", self.device))
        self.assertTrue(self.strong_user_department.has_perm("devices.view_device", self.device))

        self.assertFalse(self.weak_user.has_perm("devices.change_device", self.device))
        self.assertFalse(self.weak_user_department.has_perm("devices.change_device", self.device))
        self.assertFalse(self.strong_user.has_perm("devices.change_device", self.device))
        self.assertTrue(self.strong_user_department.has_perm("devices.change_device", self.device))

    def test_private_device_access(self):
        """
        Viewing or changing a private device requires the appropriate permission.
        On top of that, the user always needs to be from the same department as the device.
        """

        self.assertFalse(self.weak_user.has_perm("devices.view_device", self.private_device))
        self.assertFalse(self.weak_user_department.has_perm("devices.view_device", self.private_device))
        self.assertFalse(self.strong_user.has_perm("devices.view_device", self.private_device))
        self.assertTrue(self.strong_user_department.has_perm("devices.view_device", self.private_device))

        self.assertFalse(self.weak_user.has_perm("devices.change_device", self.private_device))
        self.assertFalse(self.weak_user_department.has_perm("devices.change_device", self.private_device))
        self.assertFalse(self.strong_user.has_perm("devices.change_device", self.private_device))
        self.assertTrue(self.strong_user_department.has_perm("devices.change_device", self.private_device))

    def test_public_device_access(self):
        """
        Any user, no matter what permission level, is allowed to view all public devices.
        Changing the device requires the same permissions as a non-public-non-private device.
        """
        self.assertTrue(self.weak_user.has_perm("devices.view_device", self.public_device))
        self.assertTrue(self.weak_user_department.has_perm("devices.view_device", self.public_device))
        self.assertTrue(self.strong_user.has_perm("devices.view_device", self.public_device))
        self.assertTrue(self.strong_user_department.has_perm("devices.view_device", self.public_device))

        self.assertFalse(self.weak_user.has_perm("devices.change_device", self.public_device))
        self.assertFalse(self.weak_user_department.has_perm("devices.change_device", self.public_device))
        self.assertFalse(self.strong_user.has_perm("devices.change_device", self.public_device))
        self.assertTrue(self.strong_user_department.has_perm("devices.change_device", self.public_device))


class TestDepartmentPerms(TestCase):
    def setUp(self):
        self.department = baker.make(Department)

        department_view_permission = Permission.objects.get(codename="view_department")
        department_change_permission = Permission.objects.get(codename="change_department")
        full_permissions = [department_view_permission, department_change_permission]

        self.weak_nonmember = baker.make(Lageruser)
        self.strong_nonmember = baker.make(Lageruser, user_permissions=full_permissions)

        self.weak_department_admin = baker.make(Lageruser, departments=[self.department])
        self.strong_department_admin = baker.make(
            Lageruser, departments=[self.department], user_permissions=full_permissions
        )

        self.weak_department_member = baker.make(Lageruser, departments=[self.department])
        self.strong_department_member = baker.make(
            Lageruser, departments=[self.department], user_permissions=full_permissions
        )

        for user in {self.weak_department_admin, self.strong_department_admin}:
            user.departmentuser_set.update(role="a")

        for user in {self.weak_department_member, self.strong_department_member}:
            user.departmentuser_set.update(role="m")

    def test_department_perms_nonmember(self):
        """
        A user who is not a member of a department can neither view nor change it.
        """

        self.assertFalse(self.weak_department_member.has_perm("users.view_department", self.department))
        self.assertFalse(self.strong_department_member.has_perm("users.view_department", self.department))

        self.assertFalse(self.weak_department_member.has_perm("users.change_department", self.department))
        self.assertFalse(self.strong_department_member.has_perm("users.change_department", self.department))

    def test_department_perms_member(self):
        """
        A member user can neither view nor change departments they are a part of.
        """
        self.assertFalse(self.weak_department_member.has_perm("users.view_department", self.department))
        self.assertFalse(self.strong_department_member.has_perm("users.view_department", self.department))

        self.assertFalse(self.weak_department_member.has_perm("users.change_department", self.department))
        self.assertFalse(self.strong_department_member.has_perm("users.change_department", self.department))

    def test_department_perms_admin(self):
        """
        An admin user can view and change departments they are a part of,
        as long as they also have the corresponding permission.
        """

        self.assertFalse(self.weak_department_admin.has_perm("users.view_department", self.department))
        self.assertTrue(self.strong_department_admin.has_perm("users.view_department", self.department))

        self.assertFalse(self.weak_department_admin.has_perm("users.change_department", self.department))
        self.assertTrue(self.strong_department_admin.has_perm("users.change_department", self.department))
