import datetime

from django.utils.translation import gettext_lazy as _
from reversion.models import Revision

from lagerregal.devicegroups.models import Devicegroup
from lagerregal.devices.models import Device
from lagerregal.devices.models import Lending
from lagerregal.locations.models import Section


def get_progresscolor(percent):
    if percent > 90:
        return "danger"
    elif percent > 60:
        return "warning"
    else:
        return "success"


class Widget:
    canonical_name: str
    readable_name: str

    @staticmethod
    def get_user_revisions(user, only_user):
        if only_user:
            return Revision.objects.filter(user=user)
        else:
            return Revision.objects.all()

    @staticmethod
    def get_user_lendings(user, only_user):
        if only_user:
            departments = user.departments.all()
            return Lending.objects.select_related("device", "owner").filter(device__department__in=departments)
        else:
            return Lending.objects.select_related("device", "owner")

    @staticmethod
    def get_user_devices(user, only_user):
        if only_user:
            departments = user.departments.all()
            return Device.active().filter(department__in=departments)
        else:
            return Device.active()

    @classmethod
    def get_context_data(cls, user, only_user=False):
        return {}


class StatisticsWidget(Widget):
    canonical_name = "statistics"
    readable_name = _("Statistics")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        context = {}

        devices = cls.get_user_devices(user, only_user)

        context["device_all"] = devices.count()
        if context["device_all"] != 0:
            context["device_available"] = Device.active().filter(currentlending=None).count()
            context["device_percent"] = 100 - int(
                float(context["device_available"]) / context["device_all"] * 100
            )
            context["device_percentcolor"] = get_progresscolor(context["device_percent"])

        return context


class EditHistoryWidget(Widget):
    canonical_name = "edithistory"
    readable_name = _("Edit History")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        revisions = cls.get_user_revisions(user, only_user)

        return {
            "revisions": (
                revisions
                .select_related("user")
                .prefetch_related("version_set", "version_set__content_type")
                .order_by("-date_created")[:20]
            )
        }


class NewestDevicesWidget(Widget):
    canonical_name = "newestdevices"
    readable_name = _("Newest Devices")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        devices = cls.get_user_devices(user, only_user)
        return {"newest_devices": devices.order_by("-pk")[:10]}


class OverdueDevicesWidget(Widget):
    canonical_name = "overdue"
    readable_name = _("Overdue Devices")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        context = {"today": datetime.date.today()}

        lendings = cls.get_user_lendings(user, only_user)
        context["overdue"] = lendings.filter(duedate__lt=context["today"], returndate=None).order_by("duedate")[:10]

        return context


class GroupsWidget(Widget):
    canonical_name = "groups"
    readable_name = _("Groups")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        return {"groups": Devicegroup.objects.all()}


class SectionsWidget(Widget):
    canonical_name = "sections"
    readable_name = _("Sections")

    @classmethod
    def get_context_data(cls,user, only_user=False):
        return {"sections": Section.objects.all()}


class RecentLendingsWidget(Widget):
    canonical_name = "recentlendings"
    readable_name = _("Recent Lendings")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        lendings = cls.get_user_lendings(user, only_user)
        return {"recentlendings": lendings.all().order_by("-pk")[:10]}


class ShortTermDevicesWidget(Widget):
    canonical_name = "shorttermdevices"
    readable_name = _("Devices for short-term Lending")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        return {"shorttermdevices": Device.objects.filter(templending=True)[:10]}


class BookmarksWidget(Widget):
    canonical_name = "bookmarks"
    readable_name = _("Bookmarked Devices")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        return {"bookmarks": user.bookmarks.all()[:10]}


class ReturnSoonWidget(Widget):
    canonical_name = "returnsoon"
    readable_name = _("Devices that are due soon")

    @classmethod
    def get_context_data(cls, user, only_user=False):
        context = {"today": datetime.date.today()}

        soon = context["today"] + datetime.timedelta(days=10)
        lendings = cls.get_user_lendings(user, only_user)

        context["returnsoon"] = lendings.filter(
            duedate__lte=soon, duedate__gt=context["today"], returndate=None
        ).order_by("duedate")[:10]

        return context


WIDGETS = {
    widget_class.canonical_name: widget_class for widget_class in Widget.__subclasses__()
}
WIDGET_NAMES = [
    (widget_name, widget_class.readable_name) for widget_name, widget_class in WIDGETS.items()
]
