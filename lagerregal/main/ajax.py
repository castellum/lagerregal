import json

from django.db.models import Max
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.views.generic.base import View

from lagerregal.main.models import DashboardWidget
from lagerregal.main.widgets import WIDGETS


class WidgetAdd(View):
    def post(self, request):
        widgetname = request.POST["widgetname"]
        if widgetname in dict(WIDGETS):
            userwidgets = DashboardWidget.objects.filter(user=request.user)
            if len(userwidgets.filter(widgetname=widgetname)) != 0:
                return HttpResponse("")
            widget = DashboardWidget()
            widget.column = "l"
            oldindex = userwidgets.filter(column="l").aggregate(Max("index"))["index__max"]
            widget.index = oldindex + 1 if oldindex is not None else 1
            widget.widgetname = widgetname
            widget.user = request.user
            widget.save()

            context = WIDGETS[widgetname].get_context_data(request.user)

            return HttpResponse(render_to_string(f"snippets/widgets/{widgetname}.html", context))
        else:
            return HttpResponse("Error: invalid widget name")


class WidgetRemove(View):
    def post(self, request):
        widgetname = request.POST["widgetname"]
        if widgetname in dict(WIDGETS):
            DashboardWidget.objects.get(user=request.user, widgetname=widgetname).delete()
            return HttpResponse("")
        else:
            return HttpResponse("Error: invalid widget name")


class WidgetToggle(View):
    def post(self, request):
        widgetname = request.POST["widgetname"]
        if widgetname in dict(WIDGETS):
            w = DashboardWidget.objects.get(user=request.user, widgetname=widgetname)
            w.minimized = not w.minimized
            w.save()
            return HttpResponse("")
        else:
            return HttpResponse("Error: invalid widget name")


class WidgetMove(View):
    def post(self, request):
        userwidgets = json.loads(request.POST["widgets"])

        for widgetname, widgetattr in userwidgets.items():
            if widgetname in dict(WIDGETS):
                w = DashboardWidget.objects.get(user=request.user, widgetname=widgetname)
                if w.index != widgetattr["index"] or w.column != widgetattr["column"]:
                    w.index = widgetattr["index"]
                    w.column = widgetattr["column"]
                    w.save()
        return HttpResponse("")
