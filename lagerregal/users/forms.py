from django import forms

from lagerregal.users.models import DepartmentUser
from lagerregal.users.models import Lageruser


class AvatarForm(forms.ModelForm):
    error_css_class = "has-error"
    avatar_clear = forms.BooleanField(required=False)

    class Meta:
        model = Lageruser
        fields = ["avatar"]
        widgets = {"avatar": forms.FileInput()}


class DepartmentAddUserForm(forms.ModelForm):
    error_css_class = "has-error"

    class Meta:
        model = DepartmentUser
        widgets = {"department": forms.HiddenInput()}
        fields = "__all__"
