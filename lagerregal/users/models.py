import logging
import re
from datetime import date

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from lagerregal import utils
from lagerregal.locations.models import Room

try:
    from django_auth_ldap.backend import populate_user
    receive_populate_user = receiver(populate_user)
except ImportError:
    def receive_populate_user(fn):
        return fn


class Lageruser(AbstractUser):
    language = models.CharField(
        max_length=10, null=True, blank=True, choices=settings.LANGUAGES, default=settings.LANGUAGES[0][0]
    )
    avatar = models.ImageField(upload_to=utils.get_file_location, blank=True, null=True)
    expiration_date = models.DateField(null=True, blank=True)

    departments = models.ManyToManyField(
        "users.Department", blank=True, through="users.DepartmentUser", related_name="members"
    )

    def __str__(self):
        if self.first_name != "" and self.last_name != "":
            return f"{self.first_name} {self.last_name}"
        else:
            return self.username

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        ordering = ["username"]

    def get_absolute_url(self):
        return reverse("userprofile", kwargs={"pk": self.pk})

    def clean(self):
        # initial ldap population returns a positive int as string
        # will be set correctly later by populate_ldap_user() / ldapimport cmd
        self.expiration_date = None

    @staticmethod
    def users_from_departments(departments=[]):
        if len(departments) == 0:
            return Lageruser.objects.filter(is_active=True)
        return Lageruser.objects.filter(departments__in=departments, is_active=True)


@receive_populate_user
def populate_ldap_user(sender, signal, user, ldap_user, **kwargs):
    """
    this signal is sent after the user object has been created, to override/add specific fields

    see: https://pythonhosted.org/django-auth-ldap/users.html
    """
    if settings.DEBUG:
        logger = logging.getLogger("django_auth_ldap")
        logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)

    if "accountExpires" in ldap_user.attrs:
        expiration_date = utils.convert_ad_accountexpires(int(ldap_user.attrs["accountExpires"][0]))
        user.expiration_date = expiration_date

        if user.expiration_date and user.expiration_date < date.today():
            user.is_active = False

    auth_ldap_department_regex = getattr(settings, "AUTH_LDAP_DEPARTMENT_REGEX", None)
    if auth_ldap_department_regex is not None:
        auth_ldap_department_field = getattr(settings, "AUTH_LDAP_DEPARTMENT_REGEX", None)
        if auth_ldap_department_field:
            fullname = ldap_user.attrs["distinguishedname"][0]
            match = re.compile(auth_ldap_department_regex).search(fullname)
            if match:
                department_name = match.group(1)
                try:
                    department = Department.objects.get(name=department_name)
                except Exception:
                    department = Department(name=department_name)
                    department.save()
                # departments.all() needs an id
                # so we save a newly created user object first
                if user._state.adding:
                    user.save()
                if department not in user.departments.all():
                    DepartmentUser.objects.get_or_create(user=user, department=department, role="m")

    user.save()


class Department(models.Model):
    name = models.CharField(max_length=40, unique=True)
    short_name = models.CharField(max_length=6, null=True, blank=True)
    storage_room = models.ForeignKey(Room, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Department")
        verbose_name_plural = _("Departments")
        ordering = ["name"]
        permissions = (
            ("add_department_user", _("Can add a User to a Department")),
            ("delete_department_user", _("Can remove a User from a Department")),
        )


class DepartmentUser(models.Model):
    user = models.ForeignKey(Lageruser, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    role = models.CharField(choices=(("a", _("Admin")), ("m", _("Member"))), default="a", max_length=1)
    member_since = models.DateTimeField(auto_now_add=True)
