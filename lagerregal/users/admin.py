from django.contrib import admin

from lagerregal.users.models import Department
from lagerregal.users.models import DepartmentUser
from lagerregal.users.models import Lageruser


class LageruserAdmin(admin.ModelAdmin):
    search_fields = (
        "username",
        "first_name",
        "last_name",
    )
    list_display = ("username", "__str__", "is_superuser")


class DepartmentUserAdmin(admin.ModelAdmin):
    search_fields = (
        "user__username",
        "user__first_name",
        "user__last_name",
    )


admin.site.register(Lageruser, LageruserAdmin)
admin.site.register(DepartmentUser, DepartmentUserAdmin)
admin.site.register(Department)
