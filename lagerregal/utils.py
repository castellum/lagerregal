import uuid
from datetime import date
from datetime import timedelta

from django.conf import settings
from django.forms import Select
from django.forms import SelectMultiple
from django.test.runner import DiscoverRunner


class Select2Mixin:
    def build_attrs(self, base_attrs, extra_attrs=None):
        if extra_attrs is None:
            extra_attrs = {}

        if "class" in extra_attrs:
            extra_attrs["class"] += " select2Widget"
        else:
            extra_attrs["class"] = "select2Widget"

        return super().build_attrs(base_attrs, extra_attrs=extra_attrs)


class Select2Widget(Select2Mixin, Select):
    pass


class Select2MultipleWidget(Select2Mixin, SelectMultiple):
    pass


class PaginationMixin:
    paginate_by = 30


def get_file_location(instance=None, filename=""):
    destination = ""

    if instance:
        destination += instance.__class__.__name__.lower()

    destination += "/"
    if settings.PRODUCTION:
        ext = filename.split(".")[-1]
        destination += f"{uuid.uuid4()}.{ext}"
    else:
        destination += filename

    return destination


def convert_ad_accountexpires(timestamp):
    """
    returns a datetime.date object
    returns None when timestamp is 0, larger than date.max or on another error
    """
    if timestamp is None or timestamp == 0:
        return None
    epoch_start = date(year=1601, month=1, day=1)
    seconds_since_epoch = timestamp / 10**7
    try:
        # ad timestamp can be > than date.max, return None (==never expires)
        new_date = epoch_start + timedelta(seconds=seconds_since_epoch)
        return new_date
    except OverflowError:
        return None
    except Exception:
        print(f'Cannot convert expiration_date "{timestamp}", falling back to None')


class DetectableTestRunner(DiscoverRunner):
    def __init__(self, *args, **kwargs):
        settings.TEST_MODE = True
        super().__init__(*args, **kwargs)
