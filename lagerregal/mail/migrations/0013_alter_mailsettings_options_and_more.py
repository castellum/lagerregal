# Generated by Django 4.2.11 on 2024-12-04 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0012_mailsettings'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mailsettings',
            options={'verbose_name': 'Mail Setting', 'verbose_name_plural': 'Mail Settings'},
        ),
        migrations.AddField(
            model_name='deviceupdatemail',
            name='failed_recipients',
            field=models.JSONField(default=list, verbose_name='Recipient'),
        ),
    ]
