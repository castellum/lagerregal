import traceback
from collections import defaultdict

from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from lagerregal.users.models import Lageruser


class DeviceUpdateMail(models.Model):
    subject = models.CharField(_("Subject"), max_length=500)
    body = models.CharField(_("Body"), max_length=10000)
    sent_by = models.ForeignKey(Lageruser, null=True, on_delete=models.SET_NULL)
    unsent_recipients = models.JSONField(_("Recipient"), default=list)
    failed_recipients = models.JSONField(_("Recipient"), default=list)
    created_at = models.DateTimeField(auto_now_add=True)
    device = models.ForeignKey("devices.Device", null=True, on_delete=models.CASCADE)

    def send_individual(self):
        if not self.unsent_recipients:
            return

        try:
            send_mail(self.subject, self.body, None, self.unsent_recipients)
            self.unsent_recipients = []
            self.save()
        except Exception as e:
            self.failed_recipients = self.unsent_recipients
            self.unsent_recipients = []
            self.save()
            raise e

    @classmethod
    def create_email(cls, subject, body, sent_by, recipient_emails, device, force_send=False):
        ret = cls.objects.create(
            subject=subject,
            body=body,
            sent_by=sent_by,
            unsent_recipients=recipient_emails,
            device=device,
        )
        if force_send:
            ret.send_individual()
            return ret

        inventory_mode_setting = MailSettings.objects.filter(setting_name="inventory_mode").first()
        if not inventory_mode_setting or not inventory_mode_setting.setting_enabled:
            ret.send_individual()

        return ret

    @classmethod
    def send_all_unsent(cls):
        mails_by_recipient = defaultdict(list)
        all_mails = list(cls.objects.exclude(unsent_recipients__exact=[]).order_by("created_at"))

        for unsent_mail in all_mails:
            for recipient in unsent_mail.unsent_recipients:
                mails_by_recipient[recipient].append(unsent_mail)

        for recipient, unsent_mails in mails_by_recipient.items():
            if len(unsent_mails) == 1:
                only_mail = unsent_mails[0]
                try:
                    send_mail(only_mail.subject, only_mail.body, None, only_mail.unsent_recipients)
                except Exception:
                    traceback.print_exc()
                    only_mail.failed_recipients.append(recipient)
                only_mail.unsent_recipients.remove(recipient)
                continue

            combined_mail_subject = "Multiple updates have been made to devices during an inventorying phase."
            combined_mail_body = "Multiple updates have been made to devices during an inventorying phase."
            for unsent_mail in unsent_mails:
                combined_mail_body += "\n\n---\n\n"
                combined_mail_body += f"Change made at: {unsent_mail.created_at.strftime('%Y-%m-%d %H:%M:%S')}"
                combined_mail_body += "\n\n"
                combined_mail_body += unsent_mail.subject

                unsent_mail.unsent_recipients.remove(recipient)

            try:
                send_mail(combined_mail_subject, combined_mail_body, None, [recipient])
            except Exception:
                traceback.print_exc()
                for unsent_mail in unsent_mails:
                    unsent_mail.failed_recipients.append(recipient)

        for mail in all_mails:
            mail.save()


class MailSettings(models.Model):
    setting_name = models.CharField(
        _("Setting name"), choices=(("inventory_mode", "Inventory Mode"),), unique=True, max_length=100
    )
    setting_enabled = models.BooleanField(_("Enabled"), default=False)

    class Meta:
        verbose_name = _("Mail Setting")
        verbose_name_plural = _("Mail Settings")

    def __str__(self):
        return self.get_setting_name_display()


@receiver(post_save, sender=MailSettings, dispatch_uid="update_mail_setting")
def send_mails_if_inventory_off(sender, instance, **kwargs):
    if instance.setting_name == "inventory_mode" and not instance.setting_enabled:
        DeviceUpdateMail.send_all_unsent()
