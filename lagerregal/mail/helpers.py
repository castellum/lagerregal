import traceback

from django.conf import settings
from django.contrib import messages
from django.utils.translation import gettext_lazy as _

from lagerregal.mail.models import DeviceUpdateMail
from lagerregal.users.models import Lageruser


def group_and_user_ids_to_email(group_and_user_ids):
    user_ids = [r[1:] for r in group_and_user_ids if r[0] != "g"]
    group_ids = [r[1:] for r in group_and_user_ids if r[0] == "g"]

    return {
        *Lageruser.objects.filter(id__in=user_ids).values_list("email", flat=True),
        *Lageruser.objects.filter(groups__id__in=group_ids).values_list("email", flat=True)
    }


def send_device_mail(request, device, subject, body, recipients, manual=False):
    if not device:
        raise ValueError("Tried to send device mail without device.")
    if not subject:
        raise ValueError("Tried to send device mail without subject.")
    if not body:
        raise ValueError("Tried to send device mail without body.")
    if not recipients:
        raise ValueError("Tried to send device mail without recipients.")

    body = body.rstrip() + "\n\n---\n\n"
    body += f"Device Name: {device.name}\n"
    body += f"Inventory Number: {device.inventorynumber}\n"
    body += f"Serial Number: {device.serialnumber}\n"
    body += f"{request.build_absolute_uri(device.get_absolute_url())}\n"

    DeviceUpdateMail.create_email(subject, body, request.user, recipients, device, force_send=manual)


def send_automatic_device_update_mail(request, device, subject, body):
    if not getattr(settings, "INVENTORY_MANAGERS", False):
        return

    body += "\n\n"
    body += f"This change was made by user {request.user}."

    try:
        send_device_mail(request, device, subject, body, settings.INVENTORY_MANAGERS)
    except Exception:
        messages.error(request, _("Mail could not be sent."))
        traceback.print_exc()


def send_automatic_room_update_mail(request, device, old_room, new_room, reason=None):
    if not getattr(settings, "INVENTORY_MANAGERS", False):
        return

    if old_room == new_room:
        return

    if old_room is None:
        subject = f"Device \"{device.name}\", which previously wasn't in a room, was moved to room \"{new_room.name}\"."
    elif new_room is None:
        subject = (
            f'Device "{device.name}", which was previously in room "{old_room.name}", '
            "now no longer has a room associated with it."
        )
    else:
        subject = f'Device "{device.name}" was moved from room "{old_room.name}" to room "{new_room.name}".'

    body = subject

    if reason is not None:
        body += f"\n\n{reason}"
    body += f"\n\nThis change was made by user {request.user}."

    try:
        send_device_mail(request, device, subject, body, settings.INVENTORY_MANAGERS)
    except Exception:
        messages.error(request, _("Mail could not be sent."))
        traceback.print_exc()


def send_device_creation_mail(request, device, alternate_subject=None):
    relevant_device_fields = device.get_email_update_fields_as_dict()

    subject = alternate_subject if alternate_subject is not None else f"Device {device.name} was created"

    body = subject
    body += "\n\n"

    body += "\n".join(
        f"{field_name.title()}: {value}" for field_name, value in relevant_device_fields.items()
    )

    send_automatic_device_update_mail(request, device, subject, body)

